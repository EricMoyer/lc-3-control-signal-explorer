﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LC_3ControlSignalExplorer
{
    /// <summary>
    /// Represents the state of the LC3 system
    /// </summary>
    class LC3
    {
        /// <summary>
        /// Represents the memory of the LC3. There are 64K 16 bit words. <code>memory[address]</code> contains the word at <code>address</code> when <code>address</code> is an integer from 0...65535
        /// </summary>
        public UInt16[] memory = new UInt16[65536];

        /// <summary>
        /// The current program counter
        /// </summary>
        public UInt16 PC = new UInt16();

        /// <summary>
        /// The registers of the LC3. R0..R7 are in <code>registers[0]</code>...<code>registers[7]</code>
        /// </summary>
        public UInt16[] registers = new UInt16[8];

        /// <summary>
        /// The condition code register (holds either 'N', 'Z', or 'P')
        /// </summary>
        public Char CC = 'Z';

        /// <summary>
        /// Memory access register
        /// </summary>
        public UInt16 MAR = new UInt16();

        /// <summary>
        /// Memory data register
        /// </summary>
        public UInt16 MDR = new UInt16();

        /// <summary>
        /// Instruction register
        /// </summary>
        public UInt16 IR = new UInt16();

        /// <summary>
        /// The memory enable (MEM.EN) control signal
        /// </summary>
        public ControlSignal MEM_EN = new ControlSignal(1);

        /// <summary>
        /// The memory read/write (R.W) control signal
        /// </summary>
        public ControlSignal R_W = new ControlSignal(1);

        /// <summary>
        /// Load Memory Address Register control signal
        /// </summary>
        public ControlSignal LD_MAR = new ControlSignal(1);

        /// <summary>
        /// Load Memory Data Register control signal
        /// </summary>
        public ControlSignal LD_MDR = new ControlSignal(1);

        /// <summary>
        /// Gate Memory Data Register control signal
        /// </summary>
        public ControlSignal GateMDR = new ControlSignal(1);

        /// <summary>
        /// Gate Arithmetic Logic Unit control signal
        /// </summary>
        public ControlSignal GateALU = new ControlSignal(1);

        /// <summary>
        /// Load instruction register control signal
        /// </summary>
        public ControlSignal LD_IR = new ControlSignal(1);


        /// <summary>
        /// Load CC control signal
        /// </summary>
        public ControlSignal LD_CC = new ControlSignal(1);

        /// <summary>
        /// Arithmetic Logic Unit function selection control signal.
        /// 
        /// ADD = 00
        /// AND = 01
        /// NOT = 10
        /// illegal = 11
        /// </summary>
        public ControlSignal ALUK = new ControlSignal(2);

        /// <summary>
        /// Control signal for the multiplexer that chooses whether ALU input B is SR2 or the lower 5 bits of IR
        /// </summary>
        public ControlSignal SR2MUX = new ControlSignal(1);

        /// <summary>
        /// Select signal for mux choosing PC or SR1 (which will be added to the final address)
        /// </summary>
        public ControlSignal ADDR1MUX = new ControlSignal(1);

        /// <summary>
        /// Control signal for mux choosing between different components that will be added to produce the final address
        /// </summary>
        public ControlSignal ADDR2MUX = new ControlSignal(2);

        /// <summary>
        /// 3 bits for choosing which register is source register 1
        /// </summary>
        public ControlSignal SR1 = new ControlSignal(3);

        /// <summary>
        /// 3 bits for choosing which register is source register 2
        /// </summary>
        public ControlSignal SR2 = new ControlSignal(3);

        /// <summary>
        /// 3 bits for choosing the destination register
        /// </summary>
        public ControlSignal DR = new ControlSignal(3);

        /// <summary>
        /// Load Register control signal. Destination register is loaded from bus if true.
        /// </summary>
        public ControlSignal LD_REG = new ControlSignal(1);

        /// <summary>
        /// Selects whether PC is loaded from the bus, the calculated address, or from its own next value.
        /// </summary>
        public ControlSignal PCMUX = new ControlSignal(2);

        /// <summary>
        /// Selects whether the calculated address to put on the bus comes from the big adder or the zero extended lower 8 bits of the instruction
        /// </summary>
        public ControlSignal MARMUX = new ControlSignal(1);

        /// <summary>
        /// Load PC control signal. 
        /// </summary>
        public ControlSignal LD_PC = new ControlSignal(1);

        /// <summary>
        /// True if the output of MARMUX is put on the bus
        /// </summary>
        public ControlSignal GateMARMUX = new ControlSignal(1);

        /// <summary>
        /// True if the PC is put on the bus
        /// </summary>
        public ControlSignal GatePC = new ControlSignal(1);

        /// <summary>
        /// Updates the state as if time had been spent to wait for the memory controller to signal it was finished with its operations.
        /// 
        /// Also calls stepClock (since that will take at least one clock-cycle).
        /// </summary>
        public void waitForMemoryToComplete()
        {
            stepClock();
            if (MEM_EN.asUInt16 != 0)
            {
                if (R_W.asUInt16 == 0)
                { //Memory read
                    MDR = memory[MAR];
                }
                else
                { //Memory write
                    memory[MAR] = MDR;
                }
            }
        }

        /// <summary>
        /// Advances the clock one step, causing the registers to be loaded with their new values.
        /// </summary>
        public void stepClock()
        {
            // Teal with system bus state - since many registers load from the system bus
            ControlSignal sysBus = new ControlSignal(16, SystemBusCanBeError());
            Boolean validSysBus = sysBus.isBitString;
            UInt16 sysBusVal = sysBus.asUInt16; //Make sure same random value assigned if there is an error on the system bus

            // Calculate the new values
            UInt16 newIR = IR;
            if (LD_IR.asUInt16 != 0)
            {
                newIR = sysBusVal;
            }

            Char newCC = CC;
            if(LD_CC.asUInt16 != 0)
            {
                if(sysBusVal == 0){
                    newCC = 'Z';
                }else if(sysBusVal < 0x7FFF)
                {
                    newCC = 'P';
                }else{
                    newCC = 'N';
                }
            }

            UInt16 newMDR = MDR;
            if(LD_MDR.asUInt16 != 0)
            {
                newMDR = sysBusVal;
            }

            UInt16 newMAR = MAR;
            if(LD_MAR.asUInt16 != 0)
            {
                newMAR = sysBusVal;
            }

            UInt16 newREG = registers[DR.asUInt16];
            if(LD_REG.asUInt16 != 0)
            {
                newREG = sysBusVal;
            }

            UInt16 newPC = PC;
            if(LD_PC.asUInt16 != 0)
            {
                ControlSignal pcmuxOut = new ControlSignal(16, PCMUXOutputCanBeError());
                if(pcmuxOut.isBitString){
                    newPC = pcmuxOut.asUInt16;
                }else{
                    //If it's an error, it comes from the system bus, so use that value for consistency
                    newPC = sysBusVal; 
                }
            }
            
            // Assign newly calculated values to the registers
            IR = newIR;
            CC = newCC;
            MDR = newMDR;
            MAR = newMAR;
            registers[DR.asUInt16] = newREG;
            PC = newPC;
        }

        /// <summary>
        /// Sets everything in the LC-3 except the control signals to 0 
        /// </summary>
        public void zeroMachine()
        {
            for (int i = 0; i <= 65535; ++i)
            {
                memory[i] = 0;
            }

            PC = 0;

            for (UInt16 i = 0; i <= 7; ++i)
            {
                registers[i] = 0;
            }

            CC = 'Z';

            MAR = 0;
            MDR = 0;
            IR = 0;
        }

        /// <summary>
        /// Sets everything in the LC-3 except the control signals to random values 
        /// </summary>
        /// <param name="random">The random number generator used for generating the random values</param>
        public void randomizeMachine(Random random)
        {
            for (int i = 0; i <= 65535; ++i)
            {
                memory[i] = (UInt16)random.Next(UInt16.MaxValue + 1);
            }

            PC = (UInt16)random.Next(UInt16.MaxValue + 1);

            for (int i = 0; i <= 7; ++i)
            {
                registers[i] = (UInt16)random.Next(UInt16.MaxValue + 1);
            }

            switch (random.Next(3))
            {
                case 0: CC = 'N'; break;
                case 1: CC = 'Z'; break;
                case 2: CC = 'P'; break;
            }

            MAR = (UInt16)random.Next(UInt16.MaxValue + 1);
            MDR = (UInt16)random.Next(UInt16.MaxValue + 1);
            IR = (UInt16)random.Next(UInt16.MaxValue + 1);
        }

        /// <summary>
        /// Returns the value from SR2OUT in the register file
        /// </summary>
        /// <returns>the value from SR2OUT in the register file</returns>
        public String SR2OutWire()
        {
            return (new ControlSignal(16, registers[SR2.asUInt16])).asString;
        }

        /// <summary>
        /// Returns the value from SR1OUT in the register file
        /// </summary>
        /// <returns>the value from SR1OUT in the register file</returns>
        public String SR1OutWire()
        {
            return (new ControlSignal(16, registers[SR1.asUInt16])).asString;
        }

        /// <summary>
        /// Returns the value for the output wire above the instruction register
        /// </summary>
        /// <returns>the value for the output wire above the instruction register</returns>
        public String IROutWire()
        {
            return (new ControlSignal(16, IR)).asString;
        }

        /// <summary>
        /// Returns the value of the first input to the SR2MUX
        /// </summary>
        /// <returns>the value of the first input to the SR2MUX</returns>
        public String SR2MUXInput0Wire()
        {
            return signExtend(bitSlice(IROutWire(), 4, 0));
        }

        /// <summary>
        /// Returns the value output by SR2MUX
        /// </summary>
        /// <returns>the value output by SR2MUX</returns>
        public String SR2MUXOutputWire()
        {
            if (SR2MUX.asString.Equals("0"))
            {
                return SR2MUXInput0Wire();
            }
            else
            {
                return SR2OutWire();
            }
        }

        /// <summary>
        /// Returns the value output by the ALU
        /// </summary>
        /// <returns>the value output by the ALU</returns>
        public String ALUOutputWire()
        {
            ControlSignal a = new ControlSignal(16, SR1OutWire());
            ControlSignal b = new ControlSignal(16, SR2MUXOutputWire());
            UInt16 result;
            switch (ALUK.asUInt16)
            {
                case 0:
                    result = (UInt16)((a.asUInt16 + b.asUInt16) & 0xFFFF);
                    break;
                case 1:
                    result = (UInt16)(a.asUInt16 & b.asUInt16);
                    break;
                case 2:
                    result = (UInt16)(~ a.asUInt16);
                    break;
                default:
                    result = 0;
                    break;
            }

            return (new ControlSignal(16, result)).asString;
        }

        /// <summary>
        /// Returns the first input wire to ADDR2MUX
        /// </summary>
        /// <returns>the first input wire to ADDR2MUX</returns>
        public String ADDR2MUXInput0()
        {
            return signExtend(bitSlice(IROutWire(), 10, 0));
        }

        /// <summary>
        /// Returns the second input wire to ADDR2MUX
        /// </summary>
        /// <returns>the second input wire to ADDR2MUX</returns>
        public String ADDR2MUXInput1()
        {
            return signExtend(bitSlice(IROutWire(), 8, 0));
        }

        /// <summary>
        /// Returns the third input wire to ADDR2MUX
        /// </summary>
        /// <returns>the third input wire to ADDR2MUX</returns>
        public String ADDR2MUXInput2()
        {
            return signExtend(bitSlice(IROutWire(), 5, 0));
        }

        /// <summary>
        /// Return the output of ADDR2MUX
        /// </summary>
        /// <returns>the output of ADDR2MUX</returns>
        public String ADDR2MUXOutputWire()
        {
            switch (ADDR2MUX.asUInt16)
            {
                case 0:
                    return ADDR2MUXInput0();
                case 1:
                    return ADDR2MUXInput1();
                case 2:
                    return ADDR2MUXInput2();
                case 3:
                    return new String('0', 16);

                default:
                    throw new ArgumentOutOfRangeException("ADDR2MUX", "The ADDR2MUX control signal should only be able to take on binary values from 00..11");
            }
        }

        /// <summary>
        /// Return the value on the wires coming out of the PC register
        /// </summary>
        /// <returns>the value on the wires coming out of the PC register</returns>
        public String PCOutputWire()
        {
            return (new ControlSignal(16, PC)).asString;
        }

        /// <summary>
        /// Returns the value output by ADDR1MUX
        /// </summary>
        /// <returns>the value output by ADDR1MUX</returns>
        public String ADDR1MUXOutput()
        {
            switch (ADDR1MUX.asUInt16)
            {
                case 0:
                    return SR1OutWire();
                case 1:
                    return PCOutputWire();

                default:
                    throw new ArgumentOutOfRangeException("ADDR1MUX", "The ADDR1MUX control signal should only be able to take on binary values from 0..1");
            }
        }

        /// <summary>
        /// Return the result of the adder whose inputs are ADDR1MUX and ADDR2MUX
        /// </summary>
        /// <returns>the result of the adder whose inputs are ADDR1MUX and ADDR2MUX</returns>
        public String ADDRAdderOutput()
        {
            ControlSignal a = new ControlSignal(16, ADDR1MUXOutput());
            ControlSignal b = new ControlSignal(16, ADDR2MUXOutputWire());
            UInt16 result = (UInt16)((a.asUInt16 + b.asUInt16) & 0xFFFF);
            return (new ControlSignal(16, result)).asString;
        }

        /// <summary>
        /// Return the value on the first input to MARMUX
        /// </summary>
        /// <returns>the value on the first input to MARMUX</returns>
        public String MARMUXInput0()
        {
            return bitSlice(zeroExtend(IROutWire()), 7, 0);
        }

        /// <summary>
        /// Return the output of MARMUX
        /// </summary>
        /// <returns>the output of MARMUX</returns>
        public String MARMUXOutput()
        {
            switch (MARMUX.asUInt16)
            {
                case 0:
                    return MARMUXInput0();
                case 1:
                    return ADDRAdderOutput();

                default:
                    throw new ArgumentOutOfRangeException("MARMUX", "The MARMUX control signal should only be able to take on binary values from 0..1");
            }
        }


        /// <summary>
        /// Return the value on the wire coming out of the program counter incrementer (shown as +1 on the diagram)
        /// </summary>
        /// <returns>the value on the wire coming out of the program counter incrementer (shown as +1 on the diagram)</returns>
        public String PCIncWire()
        {
            ControlSignal pc = new ControlSignal(16, PCOutputWire());
            UInt16 result = (UInt16)((pc.asUInt16 + 1) & 0xFFFF);
            return (new ControlSignal(16, result)).asString;            
        }

        /// <summary>
        /// Return the value on the output of the MDR
        /// </summary>
        /// <returns>the value on the output of the MDR</returns>
        public String MDROutputWire()
        {
            return (new ControlSignal(16, MDR)).asString;
        }


        /// <summary>
        /// Return the value on the system bus. Note that this is not necessarily a valid bit string it can be "Undefined" or "Short Circuit"
        /// </summary>
        /// <returns>the value on the system bus. Note that this is not necessarily a valid bit string it can be "Undefined" or "Short Circuit"</returns>
        public String SystemBusCanBeError()
        {
            String result = "Undefined";

            if (GatePC.asUInt16 != 0)
            {
                if (result.Equals("Undefined"))
                {
                    result = PCOutputWire();
                }
                else
                {
                    result = "Short Circuit";
                }
            }

            if (GateMARMUX.asUInt16 != 0)
            {
                if (result.Equals("Undefined"))
                {
                    result = MARMUXOutput();
                }
                else
                {
                    result = "Short Circuit";
                }
            }

            if (GateALU.asUInt16 != 0)
            {
                if (result.Equals("Undefined"))
                {
                    result = ALUOutputWire();
                }
                else
                {
                    result = "Short Circuit";
                }
            }

            if (GateMDR.asUInt16 != 0)
            {
                if (result.Equals("Undefined"))
                {
                    result = MDROutputWire();
                }
                else
                {
                    result = "Short Circuit";
                }
            }

            return result;
        }

        /// <summary>
        /// Return the output of PCMUX. Since PCMUX takes input from the System Bus (which can be short-circuited or undefined) it is possible for this to return an error output string.
        /// </summary>
        /// <returns>the output of PCMUX. Since PCMUX takes input from the System Bus (which can be short-circuited or undefined) it is possible for this to return an error output string.</returns>
        public String PCMUXOutputCanBeError()
        {
            switch(PCMUX.asUInt16)
            {
                case 0:
                    return SystemBusCanBeError();
                case 1:
                    return ADDRAdderOutput();
                case 2:
                    return PCIncWire();
                case 3:
                    return new String('0', 16);
                default:
                    throw new ArgumentException("PCMUX control signal should only be able to hold binary values 00..11", "PCMUX");
            }
        }
        
        /// <summary>
        /// Sign extends a bit-string to 16 bits
        /// </summary>
        /// <param name="input">The input bit string</param>
        /// <returns><code>input</code> sign extended to 16 bits</returns>
        String signExtend(String input)
        {
            if (input.Length < 16)
            {
                return (new String(input[0], 16 - input.Length)) + input;
            }
            else if (input.Length == 16)
            {
                return input;
            }
            else
            {
                throw new ArgumentOutOfRangeException("input", "signExtend must have input bit strings at most 16 characters long.");
            }
        }

        /// <summary>
        /// Zero extends a bit-string to 16 bits
        /// </summary>
        /// <param name="input">The input bit string</param>
        /// <returns><code>input</code> zero extended to 16 bits</returns>
        String zeroExtend(String input)
        {
            if (input.Length < 16)
            {
                return (new String('0', 16 - input.Length)) + input;
            }
            else if (input.Length == 16)
            {
                return input;
            }
            else
            {
                throw new ArgumentOutOfRangeException("input", "zeroExtend must have input bit strings at most 16 characters long.");
            }
        }

        /// <summary>
        /// Allows taking substrings of bit strings using bit numbers (that number with 0 on the right) in the same syntax as that used in the diagram
        /// </summary>
        /// <param name="input">The bit string to slice</param>
        /// <param name="first">The bit number of the starting (left-most) bit. Must be at least as large as last.</param>
        /// <param name="last">The bit number of the ending (right-most) bit. Cannot be larger than first.</param>
        /// <returns>The given bits in the bit-string</returns>
        String bitSlice(String input, UInt16 first, UInt16 last)
        {
            if(first < last)
            {
                throw new ArgumentException("first must be at least as large as last (larger numbered bits are to the left)", "first");
            }
            else if (first > (input.Length-1))
            {
                throw new ArgumentOutOfRangeException("first", "Trying to take a bit slice of a bit string that is too short.");
            }

            return input.Substring(input.Length - 1 - first, first - last + 1);
        }
    }
}
