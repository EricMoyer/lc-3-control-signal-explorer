﻿namespace LC_3ControlSignalExplorer
{
    partial class LC3ControlSignalExplorerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LC3ControlSignalExplorerForm));
            this.GateMARMUXTextBox = new System.Windows.Forms.MaskedTextBox();
            this.GateMARMUXLabel = new System.Windows.Forms.Label();
            this.GatePCLabel = new System.Windows.Forms.Label();
            this.GatePCTextBox = new System.Windows.Forms.MaskedTextBox();
            this.MARMUXLabel = new System.Windows.Forms.Label();
            this.MARMUXTextBox = new System.Windows.Forms.MaskedTextBox();
            this.LD_PCLabel = new System.Windows.Forms.Label();
            this.LD_PCTextBox = new System.Windows.Forms.MaskedTextBox();
            this.DRLabel = new System.Windows.Forms.Label();
            this.DRTextBox = new System.Windows.Forms.MaskedTextBox();
            this.PCMUXLabel = new System.Windows.Forms.Label();
            this.PCMUXTextBox = new System.Windows.Forms.MaskedTextBox();
            this.LD_REGLabel = new System.Windows.Forms.Label();
            this.LD_REGTextBox = new System.Windows.Forms.MaskedTextBox();
            this.SR1Label = new System.Windows.Forms.Label();
            this.SR1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.SR2Label = new System.Windows.Forms.Label();
            this.SR2TextBox = new System.Windows.Forms.MaskedTextBox();
            this.ADDR1MUXLabel = new System.Windows.Forms.Label();
            this.ADDR1MUXTextBox = new System.Windows.Forms.MaskedTextBox();
            this.ADDR2MUXLabel = new System.Windows.Forms.Label();
            this.ADDR2MUXTextBox = new System.Windows.Forms.MaskedTextBox();
            this.SR2MUXLabel = new System.Windows.Forms.Label();
            this.SR2MUXTextBox = new System.Windows.Forms.MaskedTextBox();
            this.ALUKLabel = new System.Windows.Forms.Label();
            this.ALUKTextBox = new System.Windows.Forms.MaskedTextBox();
            this.LD_IRLabel = new System.Windows.Forms.Label();
            this.LD_IRTextBox = new System.Windows.Forms.MaskedTextBox();
            this.LD_CCLabel = new System.Windows.Forms.Label();
            this.LD_CCTextBox = new System.Windows.Forms.MaskedTextBox();
            this.GateALULabel = new System.Windows.Forms.Label();
            this.GateALUTextBox = new System.Windows.Forms.MaskedTextBox();
            this.GateMDRLabel = new System.Windows.Forms.Label();
            this.GateMDRTextBox = new System.Windows.Forms.MaskedTextBox();
            this.LD_MDRLabel = new System.Windows.Forms.Label();
            this.LD_MDRTextBox = new System.Windows.Forms.MaskedTextBox();
            this.LD_MARLabel = new System.Windows.Forms.Label();
            this.LD_MARTextBox = new System.Windows.Forms.MaskedTextBox();
            this.MEM_ENLabel = new System.Windows.Forms.Label();
            this.MEM_ENTextBox = new System.Windows.Forms.MaskedTextBox();
            this.R_WLabel = new System.Windows.Forms.Label();
            this.R_WTextBox = new System.Windows.Forms.MaskedTextBox();
            this.MARMUXInput0Label = new System.Windows.Forms.Label();
            this.SR2OutWireLabel = new System.Windows.Forms.Label();
            this.SR1OutWireLabel = new System.Windows.Forms.Label();
            this.IROutWireLabel = new System.Windows.Forms.Label();
            this.SR2MUXInput0WireLabel = new System.Windows.Forms.Label();
            this.SR2MUXOutputWireLabel = new System.Windows.Forms.Label();
            this.ALUOutputWireLabel = new System.Windows.Forms.Label();
            this.SystemBusCanBeErrorLabel = new System.Windows.Forms.Label();
            this.ADDR2MUXInput0Label = new System.Windows.Forms.Label();
            this.ADDR2MUXInput1Label = new System.Windows.Forms.Label();
            this.ADDR2MUXInput2Label = new System.Windows.Forms.Label();
            this.ADDR2MUXOutputWireLabel = new System.Windows.Forms.Label();
            this.ADDR1MUXOutputLabel = new System.Windows.Forms.Label();
            this.ADDRAdderOutputLabel = new System.Windows.Forms.Label();
            this.PCOutputWireLabel = new System.Windows.Forms.Label();
            this.MARMUXOutputLabel = new System.Windows.Forms.Label();
            this.PCIncWireLabel = new System.Windows.Forms.Label();
            this.PCMUXOutputCanBeErrorLabel = new System.Windows.Forms.Label();
            this.MDROutputWireLabel = new System.Windows.Forms.Label();
            this.WaitOneCycleButton = new System.Windows.Forms.Button();
            this.WaitForMemoryButton = new System.Windows.Forms.Button();
            this.ControlSignalsGroupBox = new System.Windows.Forms.GroupBox();
            this.RegistersGroupBox = new System.Windows.Forms.GroupBox();
            this.CCTextBox = new System.Windows.Forms.TextBox();
            this.CCLabel = new System.Windows.Forms.Label();
            this.MDRTextBox = new System.Windows.Forms.TextBox();
            this.MDRLabel = new System.Windows.Forms.Label();
            this.MARTextBox = new System.Windows.Forms.TextBox();
            this.MARLabel = new System.Windows.Forms.Label();
            this.IRTextBox = new System.Windows.Forms.TextBox();
            this.IRLabel = new System.Windows.Forms.Label();
            this.PCTextBox = new System.Windows.Forms.TextBox();
            this.PCLabel = new System.Windows.Forms.Label();
            this.R7TextBox = new System.Windows.Forms.TextBox();
            this.R7Label = new System.Windows.Forms.Label();
            this.R6TextBox = new System.Windows.Forms.TextBox();
            this.R6Label = new System.Windows.Forms.Label();
            this.R5TextBox = new System.Windows.Forms.TextBox();
            this.R5Label = new System.Windows.Forms.Label();
            this.R4TextBox = new System.Windows.Forms.TextBox();
            this.R4Label = new System.Windows.Forms.Label();
            this.R3TextBox = new System.Windows.Forms.TextBox();
            this.R3Label = new System.Windows.Forms.Label();
            this.R2TextBox = new System.Windows.Forms.TextBox();
            this.R2Label = new System.Windows.Forms.Label();
            this.R1TextBox = new System.Windows.Forms.TextBox();
            this.R1Label = new System.Windows.Forms.Label();
            this.R0TextBox = new System.Windows.Forms.TextBox();
            this.R0Label = new System.Windows.Forms.Label();
            this.MemoryGroupBox = new System.Windows.Forms.GroupBox();
            this.MemoryContents3TextBox = new System.Windows.Forms.TextBox();
            this.MemoryContents2TextBox = new System.Windows.Forms.TextBox();
            this.MemoryContents1TextBox = new System.Windows.Forms.TextBox();
            this.MemoryContents0TextBox = new System.Windows.Forms.TextBox();
            this.MemoryAddr3Label = new System.Windows.Forms.Label();
            this.MemoryAddr2Label = new System.Windows.Forms.Label();
            this.MemoryAddr1Label = new System.Windows.Forms.Label();
            this.MemoryAddr0TextBox = new System.Windows.Forms.TextBox();
            this.RandomizeMachineButton = new System.Windows.Forms.Button();
            this.ZeroMachineButton = new System.Windows.Forms.Button();
            this.ControlSignalInterpretationGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataPathPictureBox = new System.Windows.Forms.PictureBox();
            this.IconAttributionLinkLabel = new System.Windows.Forms.LinkLabel();
            this.ControlSignalsGroupBox.SuspendLayout();
            this.RegistersGroupBox.SuspendLayout();
            this.MemoryGroupBox.SuspendLayout();
            this.ControlSignalInterpretationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataPathPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // GateMARMUXTextBox
            // 
            this.GateMARMUXTextBox.Location = new System.Drawing.Point(90, 128);
            this.GateMARMUXTextBox.Mask = "0";
            this.GateMARMUXTextBox.Name = "GateMARMUXTextBox";
            this.GateMARMUXTextBox.Size = new System.Drawing.Size(26, 20);
            this.GateMARMUXTextBox.TabIndex = 5;
            this.GateMARMUXTextBox.TextChanged += new System.EventHandler(this.GateMARMUXTextBox_TextChanged);
            this.GateMARMUXTextBox.Leave += new System.EventHandler(this.GateMARMUXTextBox_Leave);
            // 
            // GateMARMUXLabel
            // 
            this.GateMARMUXLabel.AutoSize = true;
            this.GateMARMUXLabel.Location = new System.Drawing.Point(6, 131);
            this.GateMARMUXLabel.Name = "GateMARMUXLabel";
            this.GateMARMUXLabel.Size = new System.Drawing.Size(78, 13);
            this.GateMARMUXLabel.TabIndex = 2;
            this.GateMARMUXLabel.Text = "GateMARMUX";
            // 
            // GatePCLabel
            // 
            this.GatePCLabel.AutoSize = true;
            this.GatePCLabel.Location = new System.Drawing.Point(6, 176);
            this.GatePCLabel.Name = "GatePCLabel";
            this.GatePCLabel.Size = new System.Drawing.Size(44, 13);
            this.GatePCLabel.TabIndex = 4;
            this.GatePCLabel.Text = "GatePC";
            // 
            // GatePCTextBox
            // 
            this.GatePCTextBox.Location = new System.Drawing.Point(90, 173);
            this.GatePCTextBox.Mask = "0";
            this.GatePCTextBox.Name = "GatePCTextBox";
            this.GatePCTextBox.Size = new System.Drawing.Size(26, 20);
            this.GatePCTextBox.TabIndex = 7;
            this.GatePCTextBox.TextChanged += new System.EventHandler(this.GatePCTextBox_TextChanged);
            this.GatePCTextBox.Leave += new System.EventHandler(this.GatePCTextBox_Leave);
            // 
            // MARMUXLabel
            // 
            this.MARMUXLabel.AutoSize = true;
            this.MARMUXLabel.Location = new System.Drawing.Point(6, 333);
            this.MARMUXLabel.Name = "MARMUXLabel";
            this.MARMUXLabel.Size = new System.Drawing.Size(55, 13);
            this.MARMUXLabel.TabIndex = 6;
            this.MARMUXLabel.Text = "MARMUX";
            // 
            // MARMUXTextBox
            // 
            this.MARMUXTextBox.Location = new System.Drawing.Point(90, 330);
            this.MARMUXTextBox.Mask = "0";
            this.MARMUXTextBox.Name = "MARMUXTextBox";
            this.MARMUXTextBox.Size = new System.Drawing.Size(26, 20);
            this.MARMUXTextBox.TabIndex = 14;
            this.MARMUXTextBox.TextChanged += new System.EventHandler(this.MARMUXTextBox_TextChanged);
            this.MARMUXTextBox.Leave += new System.EventHandler(this.MARMUXTextBox_Leave);
            // 
            // LD_PCLabel
            // 
            this.LD_PCLabel.AutoSize = true;
            this.LD_PCLabel.Location = new System.Drawing.Point(6, 288);
            this.LD_PCLabel.Name = "LD_PCLabel";
            this.LD_PCLabel.Size = new System.Drawing.Size(38, 13);
            this.LD_PCLabel.TabIndex = 8;
            this.LD_PCLabel.Text = "LD.PC";
            // 
            // LD_PCTextBox
            // 
            this.LD_PCTextBox.Location = new System.Drawing.Point(90, 285);
            this.LD_PCTextBox.Mask = "0";
            this.LD_PCTextBox.Name = "LD_PCTextBox";
            this.LD_PCTextBox.Size = new System.Drawing.Size(26, 20);
            this.LD_PCTextBox.TabIndex = 12;
            this.LD_PCTextBox.TextChanged += new System.EventHandler(this.LD_PCTextBox_TextChanged);
            this.LD_PCTextBox.Leave += new System.EventHandler(this.LD_PCTextBox_Leave);
            // 
            // DRLabel
            // 
            this.DRLabel.AutoSize = true;
            this.DRLabel.Location = new System.Drawing.Point(6, 86);
            this.DRLabel.Name = "DRLabel";
            this.DRLabel.Size = new System.Drawing.Size(23, 13);
            this.DRLabel.TabIndex = 10;
            this.DRLabel.Text = "DR";
            // 
            // DRTextBox
            // 
            this.DRTextBox.Location = new System.Drawing.Point(90, 83);
            this.DRTextBox.Mask = "000";
            this.DRTextBox.Name = "DRTextBox";
            this.DRTextBox.Size = new System.Drawing.Size(26, 20);
            this.DRTextBox.TabIndex = 3;
            this.DRTextBox.TextChanged += new System.EventHandler(this.DRTextBox_TextChanged);
            this.DRTextBox.Leave += new System.EventHandler(this.DRTextBox_Leave);
            // 
            // PCMUXLabel
            // 
            this.PCMUXLabel.AutoSize = true;
            this.PCMUXLabel.Location = new System.Drawing.Point(6, 377);
            this.PCMUXLabel.Name = "PCMUXLabel";
            this.PCMUXLabel.Size = new System.Drawing.Size(45, 13);
            this.PCMUXLabel.TabIndex = 12;
            this.PCMUXLabel.Text = "PCMUX";
            // 
            // PCMUXTextBox
            // 
            this.PCMUXTextBox.Location = new System.Drawing.Point(90, 374);
            this.PCMUXTextBox.Mask = "00";
            this.PCMUXTextBox.Name = "PCMUXTextBox";
            this.PCMUXTextBox.Size = new System.Drawing.Size(26, 20);
            this.PCMUXTextBox.TabIndex = 16;
            this.PCMUXTextBox.TextChanged += new System.EventHandler(this.PCMUXTextBox_TextChanged);
            this.PCMUXTextBox.Leave += new System.EventHandler(this.PCMUXTextBox_Leave);
            // 
            // LD_REGLabel
            // 
            this.LD_REGLabel.AutoSize = true;
            this.LD_REGLabel.Location = new System.Drawing.Point(6, 311);
            this.LD_REGLabel.Name = "LD_REGLabel";
            this.LD_REGLabel.Size = new System.Drawing.Size(47, 13);
            this.LD_REGLabel.TabIndex = 14;
            this.LD_REGLabel.Text = "LD.REG";
            // 
            // LD_REGTextBox
            // 
            this.LD_REGTextBox.Location = new System.Drawing.Point(90, 308);
            this.LD_REGTextBox.Mask = "0";
            this.LD_REGTextBox.Name = "LD_REGTextBox";
            this.LD_REGTextBox.Size = new System.Drawing.Size(26, 20);
            this.LD_REGTextBox.TabIndex = 13;
            this.LD_REGTextBox.TextChanged += new System.EventHandler(this.LD_REGTextBox_TextChanged);
            this.LD_REGTextBox.Leave += new System.EventHandler(this.LD_REGTextBox_Leave);
            // 
            // SR1Label
            // 
            this.SR1Label.AutoSize = true;
            this.SR1Label.Location = new System.Drawing.Point(6, 421);
            this.SR1Label.Name = "SR1Label";
            this.SR1Label.Size = new System.Drawing.Size(28, 13);
            this.SR1Label.TabIndex = 16;
            this.SR1Label.Text = "SR1";
            // 
            // SR1TextBox
            // 
            this.SR1TextBox.Location = new System.Drawing.Point(90, 418);
            this.SR1TextBox.Mask = "000";
            this.SR1TextBox.Name = "SR1TextBox";
            this.SR1TextBox.Size = new System.Drawing.Size(26, 20);
            this.SR1TextBox.TabIndex = 18;
            this.SR1TextBox.TextChanged += new System.EventHandler(this.SR1TextBox_TextChanged);
            this.SR1TextBox.Leave += new System.EventHandler(this.SR1TextBox_Leave);
            // 
            // SR2Label
            // 
            this.SR2Label.AutoSize = true;
            this.SR2Label.Location = new System.Drawing.Point(6, 443);
            this.SR2Label.Name = "SR2Label";
            this.SR2Label.Size = new System.Drawing.Size(28, 13);
            this.SR2Label.TabIndex = 18;
            this.SR2Label.Text = "SR2";
            // 
            // SR2TextBox
            // 
            this.SR2TextBox.Location = new System.Drawing.Point(90, 440);
            this.SR2TextBox.Mask = "000";
            this.SR2TextBox.Name = "SR2TextBox";
            this.SR2TextBox.Size = new System.Drawing.Size(26, 20);
            this.SR2TextBox.TabIndex = 19;
            this.SR2TextBox.TextChanged += new System.EventHandler(this.SR2TextBox_TextChanged);
            this.SR2TextBox.Leave += new System.EventHandler(this.SR2TextBox_Leave);
            // 
            // ADDR1MUXLabel
            // 
            this.ADDR1MUXLabel.AutoSize = true;
            this.ADDR1MUXLabel.Location = new System.Drawing.Point(6, 18);
            this.ADDR1MUXLabel.Name = "ADDR1MUXLabel";
            this.ADDR1MUXLabel.Size = new System.Drawing.Size(68, 13);
            this.ADDR1MUXLabel.TabIndex = 20;
            this.ADDR1MUXLabel.Text = "ADDR1MUX";
            // 
            // ADDR1MUXTextBox
            // 
            this.ADDR1MUXTextBox.Location = new System.Drawing.Point(90, 15);
            this.ADDR1MUXTextBox.Mask = "0";
            this.ADDR1MUXTextBox.Name = "ADDR1MUXTextBox";
            this.ADDR1MUXTextBox.Size = new System.Drawing.Size(26, 20);
            this.ADDR1MUXTextBox.TabIndex = 0;
            this.ADDR1MUXTextBox.TextChanged += new System.EventHandler(this.ADDR1MUXTextBox_TextChanged);
            this.ADDR1MUXTextBox.Leave += new System.EventHandler(this.ADDR1MUXTextBox_Leave);
            // 
            // ADDR2MUXLabel
            // 
            this.ADDR2MUXLabel.AutoSize = true;
            this.ADDR2MUXLabel.Location = new System.Drawing.Point(6, 41);
            this.ADDR2MUXLabel.Name = "ADDR2MUXLabel";
            this.ADDR2MUXLabel.Size = new System.Drawing.Size(68, 13);
            this.ADDR2MUXLabel.TabIndex = 22;
            this.ADDR2MUXLabel.Text = "ADDR2MUX";
            // 
            // ADDR2MUXTextBox
            // 
            this.ADDR2MUXTextBox.Location = new System.Drawing.Point(90, 38);
            this.ADDR2MUXTextBox.Mask = "00";
            this.ADDR2MUXTextBox.Name = "ADDR2MUXTextBox";
            this.ADDR2MUXTextBox.Size = new System.Drawing.Size(26, 20);
            this.ADDR2MUXTextBox.TabIndex = 1;
            this.ADDR2MUXTextBox.TextChanged += new System.EventHandler(this.ADDR2MUXTextBox_TextChanged);
            this.ADDR2MUXTextBox.Leave += new System.EventHandler(this.ADDR2MUXTextBox_Leave);
            // 
            // SR2MUXLabel
            // 
            this.SR2MUXLabel.AutoSize = true;
            this.SR2MUXLabel.Location = new System.Drawing.Point(6, 466);
            this.SR2MUXLabel.Name = "SR2MUXLabel";
            this.SR2MUXLabel.Size = new System.Drawing.Size(52, 13);
            this.SR2MUXLabel.TabIndex = 24;
            this.SR2MUXLabel.Text = "SR2MUX";
            // 
            // SR2MUXTextBox
            // 
            this.SR2MUXTextBox.Location = new System.Drawing.Point(90, 463);
            this.SR2MUXTextBox.Mask = "0";
            this.SR2MUXTextBox.Name = "SR2MUXTextBox";
            this.SR2MUXTextBox.Size = new System.Drawing.Size(26, 20);
            this.SR2MUXTextBox.TabIndex = 20;
            this.SR2MUXTextBox.TextChanged += new System.EventHandler(this.SR2MUXTextBox_TextChanged);
            this.SR2MUXTextBox.Leave += new System.EventHandler(this.SR2MUXTextBox_Leave);
            // 
            // ALUKLabel
            // 
            this.ALUKLabel.AutoSize = true;
            this.ALUKLabel.Location = new System.Drawing.Point(6, 63);
            this.ALUKLabel.Name = "ALUKLabel";
            this.ALUKLabel.Size = new System.Drawing.Size(35, 13);
            this.ALUKLabel.TabIndex = 26;
            this.ALUKLabel.Text = "ALUK";
            // 
            // ALUKTextBox
            // 
            this.ALUKTextBox.Location = new System.Drawing.Point(90, 60);
            this.ALUKTextBox.Mask = "00";
            this.ALUKTextBox.Name = "ALUKTextBox";
            this.ALUKTextBox.Size = new System.Drawing.Size(26, 20);
            this.ALUKTextBox.TabIndex = 2;
            this.ALUKTextBox.TextChanged += new System.EventHandler(this.ALUKTextBox_TextChanged);
            this.ALUKTextBox.Leave += new System.EventHandler(this.ALUKTextBox_Leave);
            // 
            // LD_IRLabel
            // 
            this.LD_IRLabel.AutoSize = true;
            this.LD_IRLabel.Location = new System.Drawing.Point(6, 221);
            this.LD_IRLabel.Name = "LD_IRLabel";
            this.LD_IRLabel.Size = new System.Drawing.Size(35, 13);
            this.LD_IRLabel.TabIndex = 28;
            this.LD_IRLabel.Text = "LD.IR";
            // 
            // LD_IRTextBox
            // 
            this.LD_IRTextBox.Location = new System.Drawing.Point(90, 218);
            this.LD_IRTextBox.Mask = "0";
            this.LD_IRTextBox.Name = "LD_IRTextBox";
            this.LD_IRTextBox.Size = new System.Drawing.Size(26, 20);
            this.LD_IRTextBox.TabIndex = 9;
            this.LD_IRTextBox.TextChanged += new System.EventHandler(this.LD_IRTextBox_TextChanged);
            this.LD_IRTextBox.Leave += new System.EventHandler(this.LD_IRTextBox_Leave);
            // 
            // LD_CCLabel
            // 
            this.LD_CCLabel.AutoSize = true;
            this.LD_CCLabel.Location = new System.Drawing.Point(6, 199);
            this.LD_CCLabel.Name = "LD_CCLabel";
            this.LD_CCLabel.Size = new System.Drawing.Size(38, 13);
            this.LD_CCLabel.TabIndex = 30;
            this.LD_CCLabel.Text = "LD.CC";
            // 
            // LD_CCTextBox
            // 
            this.LD_CCTextBox.Location = new System.Drawing.Point(90, 196);
            this.LD_CCTextBox.Mask = "0";
            this.LD_CCTextBox.Name = "LD_CCTextBox";
            this.LD_CCTextBox.Size = new System.Drawing.Size(26, 20);
            this.LD_CCTextBox.TabIndex = 8;
            this.LD_CCTextBox.TextChanged += new System.EventHandler(this.LD_CCTextBox_TextChanged);
            this.LD_CCTextBox.Leave += new System.EventHandler(this.LD_CCTextBox_Leave);
            // 
            // GateALULabel
            // 
            this.GateALULabel.AutoSize = true;
            this.GateALULabel.Location = new System.Drawing.Point(6, 108);
            this.GateALULabel.Name = "GateALULabel";
            this.GateALULabel.Size = new System.Drawing.Size(51, 13);
            this.GateALULabel.TabIndex = 32;
            this.GateALULabel.Text = "GateALU";
            // 
            // GateALUTextBox
            // 
            this.GateALUTextBox.Location = new System.Drawing.Point(90, 105);
            this.GateALUTextBox.Mask = "0";
            this.GateALUTextBox.Name = "GateALUTextBox";
            this.GateALUTextBox.Size = new System.Drawing.Size(26, 20);
            this.GateALUTextBox.TabIndex = 4;
            this.GateALUTextBox.TextChanged += new System.EventHandler(this.GateALUTextBox_TextChanged);
            this.GateALUTextBox.Leave += new System.EventHandler(this.GateALUTextBox_Leave);
            // 
            // GateMDRLabel
            // 
            this.GateMDRLabel.AutoSize = true;
            this.GateMDRLabel.Location = new System.Drawing.Point(6, 154);
            this.GateMDRLabel.Name = "GateMDRLabel";
            this.GateMDRLabel.Size = new System.Drawing.Size(55, 13);
            this.GateMDRLabel.TabIndex = 34;
            this.GateMDRLabel.Text = "GateMDR";
            // 
            // GateMDRTextBox
            // 
            this.GateMDRTextBox.Location = new System.Drawing.Point(90, 151);
            this.GateMDRTextBox.Mask = "0";
            this.GateMDRTextBox.Name = "GateMDRTextBox";
            this.GateMDRTextBox.Size = new System.Drawing.Size(26, 20);
            this.GateMDRTextBox.TabIndex = 6;
            this.GateMDRTextBox.TextChanged += new System.EventHandler(this.GateMDRTextBox_TextChanged);
            this.GateMDRTextBox.Leave += new System.EventHandler(this.GateMDRTextBox_Leave);
            // 
            // LD_MDRLabel
            // 
            this.LD_MDRLabel.AutoSize = true;
            this.LD_MDRLabel.Location = new System.Drawing.Point(6, 266);
            this.LD_MDRLabel.Name = "LD_MDRLabel";
            this.LD_MDRLabel.Size = new System.Drawing.Size(49, 13);
            this.LD_MDRLabel.TabIndex = 36;
            this.LD_MDRLabel.Text = "LD.MDR";
            // 
            // LD_MDRTextBox
            // 
            this.LD_MDRTextBox.Location = new System.Drawing.Point(90, 263);
            this.LD_MDRTextBox.Mask = "0";
            this.LD_MDRTextBox.Name = "LD_MDRTextBox";
            this.LD_MDRTextBox.Size = new System.Drawing.Size(26, 20);
            this.LD_MDRTextBox.TabIndex = 11;
            this.LD_MDRTextBox.TextChanged += new System.EventHandler(this.LD_MDRTextBox_TextChanged);
            this.LD_MDRTextBox.Leave += new System.EventHandler(this.LD_MDRTextBox_Leave);
            // 
            // LD_MARLabel
            // 
            this.LD_MARLabel.AutoSize = true;
            this.LD_MARLabel.Location = new System.Drawing.Point(6, 243);
            this.LD_MARLabel.Name = "LD_MARLabel";
            this.LD_MARLabel.Size = new System.Drawing.Size(48, 13);
            this.LD_MARLabel.TabIndex = 38;
            this.LD_MARLabel.Text = "LD.MAR";
            // 
            // LD_MARTextBox
            // 
            this.LD_MARTextBox.Location = new System.Drawing.Point(90, 240);
            this.LD_MARTextBox.Mask = "0";
            this.LD_MARTextBox.Name = "LD_MARTextBox";
            this.LD_MARTextBox.Size = new System.Drawing.Size(26, 20);
            this.LD_MARTextBox.TabIndex = 10;
            this.LD_MARTextBox.TextChanged += new System.EventHandler(this.LD_MARTextBox_TextChanged);
            this.LD_MARTextBox.Leave += new System.EventHandler(this.LD_MARTextBox_Leave);
            // 
            // MEM_ENLabel
            // 
            this.MEM_ENLabel.AutoSize = true;
            this.MEM_ENLabel.Location = new System.Drawing.Point(6, 355);
            this.MEM_ENLabel.Name = "MEM_ENLabel";
            this.MEM_ENLabel.Size = new System.Drawing.Size(50, 13);
            this.MEM_ENLabel.TabIndex = 40;
            this.MEM_ENLabel.Text = "MEM.EN";
            // 
            // MEM_ENTextBox
            // 
            this.MEM_ENTextBox.Location = new System.Drawing.Point(90, 352);
            this.MEM_ENTextBox.Mask = "0";
            this.MEM_ENTextBox.Name = "MEM_ENTextBox";
            this.MEM_ENTextBox.Size = new System.Drawing.Size(26, 20);
            this.MEM_ENTextBox.TabIndex = 15;
            this.MEM_ENTextBox.TextChanged += new System.EventHandler(this.MEM_ENTextBox_TextChanged);
            this.MEM_ENTextBox.Leave += new System.EventHandler(this.MEM_ENTextBox_Leave);
            // 
            // R_WLabel
            // 
            this.R_WLabel.AutoSize = true;
            this.R_WLabel.Location = new System.Drawing.Point(6, 399);
            this.R_WLabel.Name = "R_WLabel";
            this.R_WLabel.Size = new System.Drawing.Size(29, 13);
            this.R_WLabel.TabIndex = 42;
            this.R_WLabel.Text = "R.W";
            // 
            // R_WTextBox
            // 
            this.R_WTextBox.Location = new System.Drawing.Point(90, 396);
            this.R_WTextBox.Mask = "0";
            this.R_WTextBox.Name = "R_WTextBox";
            this.R_WTextBox.Size = new System.Drawing.Size(26, 20);
            this.R_WTextBox.TabIndex = 17;
            this.R_WTextBox.TextChanged += new System.EventHandler(this.R_WTextBox_TextChanged);
            this.R_WTextBox.Leave += new System.EventHandler(this.R_WTextBox_Leave);
            // 
            // MARMUXInput0Label
            // 
            this.MARMUXInput0Label.AutoSize = true;
            this.MARMUXInput0Label.Location = new System.Drawing.Point(42, 117);
            this.MARMUXInput0Label.Name = "MARMUXInput0Label";
            this.MARMUXInput0Label.Size = new System.Drawing.Size(37, 13);
            this.MARMUXInput0Label.TabIndex = 43;
            this.MARMUXInput0Label.Text = "MRX0";
            // 
            // SR2OutWireLabel
            // 
            this.SR2OutWireLabel.AutoSize = true;
            this.SR2OutWireLabel.Location = new System.Drawing.Point(377, 227);
            this.SR2OutWireLabel.Name = "SR2OutWireLabel";
            this.SR2OutWireLabel.Size = new System.Drawing.Size(34, 13);
            this.SR2OutWireLabel.TabIndex = 44;
            this.SR2OutWireLabel.Text = "SR20";
            // 
            // SR1OutWireLabel
            // 
            this.SR1OutWireLabel.AutoSize = true;
            this.SR1OutWireLabel.Location = new System.Drawing.Point(465, 227);
            this.SR1OutWireLabel.Name = "SR1OutWireLabel";
            this.SR1OutWireLabel.Size = new System.Drawing.Size(34, 13);
            this.SR1OutWireLabel.TabIndex = 45;
            this.SR1OutWireLabel.Text = "SR10";
            // 
            // IROutWireLabel
            // 
            this.IROutWireLabel.AutoSize = true;
            this.IROutWireLabel.Location = new System.Drawing.Point(97, 412);
            this.IROutWireLabel.Name = "IROutWireLabel";
            this.IROutWireLabel.Size = new System.Drawing.Size(30, 13);
            this.IROutWireLabel.TabIndex = 46;
            this.IROutWireLabel.Text = "IR00";
            // 
            // SR2MUXInput0WireLabel
            // 
            this.SR2MUXInput0WireLabel.AutoSize = true;
            this.SR2MUXInput0WireLabel.Location = new System.Drawing.Point(231, 329);
            this.SR2MUXInput0WireLabel.Name = "SR2MUXInput0WireLabel";
            this.SR2MUXInput0WireLabel.Size = new System.Drawing.Size(35, 13);
            this.SR2MUXInput0WireLabel.TabIndex = 47;
            this.SR2MUXInput0WireLabel.Text = "S2M0";
            // 
            // SR2MUXOutputWireLabel
            // 
            this.SR2MUXOutputWireLabel.AutoSize = true;
            this.SR2MUXOutputWireLabel.Location = new System.Drawing.Point(362, 364);
            this.SR2MUXOutputWireLabel.Name = "SR2MUXOutputWireLabel";
            this.SR2MUXOutputWireLabel.Size = new System.Drawing.Size(36, 13);
            this.SR2MUXOutputWireLabel.TabIndex = 48;
            this.SR2MUXOutputWireLabel.Text = "S2MX";
            // 
            // ALUOutputWireLabel
            // 
            this.ALUOutputWireLabel.AutoSize = true;
            this.ALUOutputWireLabel.Location = new System.Drawing.Point(434, 466);
            this.ALUOutputWireLabel.Name = "ALUOutputWireLabel";
            this.ALUOutputWireLabel.Size = new System.Drawing.Size(36, 13);
            this.ALUOutputWireLabel.TabIndex = 49;
            this.ALUOutputWireLabel.Text = "ALUO";
            // 
            // SystemBusCanBeErrorLabel
            // 
            this.SystemBusCanBeErrorLabel.AutoSize = true;
            this.SystemBusCanBeErrorLabel.Location = new System.Drawing.Point(465, 295);
            this.SystemBusCanBeErrorLabel.MinimumSize = new System.Drawing.Size(59, 0);
            this.SystemBusCanBeErrorLabel.Name = "SystemBusCanBeErrorLabel";
            this.SystemBusCanBeErrorLabel.Size = new System.Drawing.Size(59, 13);
            this.SystemBusCanBeErrorLabel.TabIndex = 50;
            this.SystemBusCanBeErrorLabel.Text = "SBUS";
            this.SystemBusCanBeErrorLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ADDR2MUXInput0Label
            // 
            this.ADDR2MUXInput0Label.AutoSize = true;
            this.ADDR2MUXInput0Label.Location = new System.Drawing.Point(56, 294);
            this.ADDR2MUXInput0Label.Name = "ADDR2MUXInput0Label";
            this.ADDR2MUXInput0Label.Size = new System.Drawing.Size(29, 13);
            this.ADDR2MUXInput0Label.TabIndex = 51;
            this.ADDR2MUXInput0Label.Text = "A2I0";
            // 
            // ADDR2MUXInput1Label
            // 
            this.ADDR2MUXInput1Label.AutoSize = true;
            this.ADDR2MUXInput1Label.Location = new System.Drawing.Point(83, 334);
            this.ADDR2MUXInput1Label.Name = "ADDR2MUXInput1Label";
            this.ADDR2MUXInput1Label.Size = new System.Drawing.Size(29, 13);
            this.ADDR2MUXInput1Label.TabIndex = 52;
            this.ADDR2MUXInput1Label.Text = "A2I1";
            // 
            // ADDR2MUXInput2Label
            // 
            this.ADDR2MUXInput2Label.AutoSize = true;
            this.ADDR2MUXInput2Label.Location = new System.Drawing.Point(101, 369);
            this.ADDR2MUXInput2Label.Name = "ADDR2MUXInput2Label";
            this.ADDR2MUXInput2Label.Size = new System.Drawing.Size(29, 13);
            this.ADDR2MUXInput2Label.TabIndex = 53;
            this.ADDR2MUXInput2Label.Text = "A2I2";
            // 
            // ADDR2MUXOutputWireLabel
            // 
            this.ADDR2MUXOutputWireLabel.AutoSize = true;
            this.ADDR2MUXOutputWireLabel.Location = new System.Drawing.Point(105, 221);
            this.ADDR2MUXOutputWireLabel.Name = "ADDR2MUXOutputWireLabel";
            this.ADDR2MUXOutputWireLabel.Size = new System.Drawing.Size(35, 13);
            this.ADDR2MUXOutputWireLabel.TabIndex = 54;
            this.ADDR2MUXOutputWireLabel.Text = "A2XO";
            // 
            // ADDR1MUXOutputLabel
            // 
            this.ADDR1MUXOutputLabel.AutoSize = true;
            this.ADDR1MUXOutputLabel.Location = new System.Drawing.Point(238, 221);
            this.ADDR1MUXOutputLabel.Name = "ADDR1MUXOutputLabel";
            this.ADDR1MUXOutputLabel.Size = new System.Drawing.Size(35, 13);
            this.ADDR1MUXOutputLabel.TabIndex = 55;
            this.ADDR1MUXOutputLabel.Text = "A1XO";
            // 
            // ADDRAdderOutputLabel
            // 
            this.ADDRAdderOutputLabel.AutoSize = true;
            this.ADDRAdderOutputLabel.Location = new System.Drawing.Point(199, 184);
            this.ADDRAdderOutputLabel.Name = "ADDRAdderOutputLabel";
            this.ADDRAdderOutputLabel.Size = new System.Drawing.Size(37, 13);
            this.ADDRAdderOutputLabel.TabIndex = 56;
            this.ADDRAdderOutputLabel.Text = "ADAD";
            // 
            // PCOutputWireLabel
            // 
            this.PCOutputWireLabel.AutoSize = true;
            this.PCOutputWireLabel.Location = new System.Drawing.Point(217, 50);
            this.PCOutputWireLabel.Name = "PCOutputWireLabel";
            this.PCOutputWireLabel.Size = new System.Drawing.Size(37, 13);
            this.PCOutputWireLabel.TabIndex = 57;
            this.PCOutputWireLabel.Text = "PCOU";
            // 
            // MARMUXOutputLabel
            // 
            this.MARMUXOutputLabel.AutoSize = true;
            this.MARMUXOutputLabel.Location = new System.Drawing.Point(112, 49);
            this.MARMUXOutputLabel.Name = "MARMUXOutputLabel";
            this.MARMUXOutputLabel.Size = new System.Drawing.Size(38, 13);
            this.MARMUXOutputLabel.TabIndex = 58;
            this.MARMUXOutputLabel.Text = "MARX";
            // 
            // PCIncWireLabel
            // 
            this.PCIncWireLabel.AutoSize = true;
            this.PCIncWireLabel.Location = new System.Drawing.Point(280, 170);
            this.PCIncWireLabel.Name = "PCIncWireLabel";
            this.PCIncWireLabel.Size = new System.Drawing.Size(33, 13);
            this.PCIncWireLabel.TabIndex = 59;
            this.PCIncWireLabel.Text = "PC++";
            // 
            // PCMUXOutputCanBeErrorLabel
            // 
            this.PCMUXOutputCanBeErrorLabel.AutoSize = true;
            this.PCMUXOutputCanBeErrorLabel.Location = new System.Drawing.Point(185, 98);
            this.PCMUXOutputCanBeErrorLabel.MinimumSize = new System.Drawing.Size(65, 0);
            this.PCMUXOutputCanBeErrorLabel.Name = "PCMUXOutputCanBeErrorLabel";
            this.PCMUXOutputCanBeErrorLabel.Size = new System.Drawing.Size(65, 13);
            this.PCMUXOutputCanBeErrorLabel.TabIndex = 60;
            this.PCMUXOutputCanBeErrorLabel.Text = "PCMX";
            this.PCMUXOutputCanBeErrorLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MDROutputWireLabel
            // 
            this.MDROutputWireLabel.AutoSize = true;
            this.MDROutputWireLabel.Location = new System.Drawing.Point(15, 557);
            this.MDROutputWireLabel.Name = "MDROutputWireLabel";
            this.MDROutputWireLabel.Size = new System.Drawing.Size(40, 13);
            this.MDROutputWireLabel.TabIndex = 61;
            this.MDROutputWireLabel.Text = "MDRO";
            // 
            // WaitOneCycleButton
            // 
            this.WaitOneCycleButton.Location = new System.Drawing.Point(554, 525);
            this.WaitOneCycleButton.Name = "WaitOneCycleButton";
            this.WaitOneCycleButton.Size = new System.Drawing.Size(125, 23);
            this.WaitOneCycleButton.TabIndex = 0;
            this.WaitOneCycleButton.Text = "Wait one cycle";
            this.WaitOneCycleButton.UseVisualStyleBackColor = true;
            this.WaitOneCycleButton.Click += new System.EventHandler(this.WaitOneCycleButton_Click);
            // 
            // WaitForMemoryButton
            // 
            this.WaitForMemoryButton.Location = new System.Drawing.Point(554, 555);
            this.WaitForMemoryButton.Name = "WaitForMemoryButton";
            this.WaitForMemoryButton.Size = new System.Drawing.Size(125, 23);
            this.WaitForMemoryButton.TabIndex = 1;
            this.WaitForMemoryButton.Text = "Wait for memory";
            this.WaitForMemoryButton.UseVisualStyleBackColor = true;
            this.WaitForMemoryButton.Click += new System.EventHandler(this.WaitForMemoryButton_Click);
            // 
            // ControlSignalsGroupBox
            // 
            this.ControlSignalsGroupBox.Controls.Add(this.GatePCLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.GateMARMUXTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.GateMARMUXLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.GatePCTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.MARMUXTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.MARMUXLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_PCTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_PCLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.DRTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.DRLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.PCMUXTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.PCMUXLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_REGTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_REGLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.SR1TextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.SR1Label);
            this.ControlSignalsGroupBox.Controls.Add(this.SR2TextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.SR2Label);
            this.ControlSignalsGroupBox.Controls.Add(this.ADDR1MUXTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.ADDR1MUXLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.ADDR2MUXTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.ADDR2MUXLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.R_WLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.SR2MUXTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.R_WTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.SR2MUXLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.MEM_ENLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.ALUKTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.MEM_ENTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.ALUKLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_MARLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_IRTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_MARTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_IRLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_MDRLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_CCTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_MDRTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.LD_CCLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.GateMDRLabel);
            this.ControlSignalsGroupBox.Controls.Add(this.GateALUTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.GateMDRTextBox);
            this.ControlSignalsGroupBox.Controls.Add(this.GateALULabel);
            this.ControlSignalsGroupBox.Location = new System.Drawing.Point(550, 12);
            this.ControlSignalsGroupBox.Name = "ControlSignalsGroupBox";
            this.ControlSignalsGroupBox.Size = new System.Drawing.Size(129, 495);
            this.ControlSignalsGroupBox.TabIndex = 64;
            this.ControlSignalsGroupBox.TabStop = false;
            this.ControlSignalsGroupBox.Text = "Control Signals";
            // 
            // RegistersGroupBox
            // 
            this.RegistersGroupBox.Controls.Add(this.CCTextBox);
            this.RegistersGroupBox.Controls.Add(this.CCLabel);
            this.RegistersGroupBox.Controls.Add(this.MDRTextBox);
            this.RegistersGroupBox.Controls.Add(this.MDRLabel);
            this.RegistersGroupBox.Controls.Add(this.MARTextBox);
            this.RegistersGroupBox.Controls.Add(this.MARLabel);
            this.RegistersGroupBox.Controls.Add(this.IRTextBox);
            this.RegistersGroupBox.Controls.Add(this.IRLabel);
            this.RegistersGroupBox.Controls.Add(this.PCTextBox);
            this.RegistersGroupBox.Controls.Add(this.PCLabel);
            this.RegistersGroupBox.Controls.Add(this.R7TextBox);
            this.RegistersGroupBox.Controls.Add(this.R7Label);
            this.RegistersGroupBox.Controls.Add(this.R6TextBox);
            this.RegistersGroupBox.Controls.Add(this.R6Label);
            this.RegistersGroupBox.Controls.Add(this.R5TextBox);
            this.RegistersGroupBox.Controls.Add(this.R5Label);
            this.RegistersGroupBox.Controls.Add(this.R4TextBox);
            this.RegistersGroupBox.Controls.Add(this.R4Label);
            this.RegistersGroupBox.Controls.Add(this.R3TextBox);
            this.RegistersGroupBox.Controls.Add(this.R3Label);
            this.RegistersGroupBox.Controls.Add(this.R2TextBox);
            this.RegistersGroupBox.Controls.Add(this.R2Label);
            this.RegistersGroupBox.Controls.Add(this.R1TextBox);
            this.RegistersGroupBox.Controls.Add(this.R1Label);
            this.RegistersGroupBox.Controls.Add(this.R0TextBox);
            this.RegistersGroupBox.Controls.Add(this.R0Label);
            this.RegistersGroupBox.Location = new System.Drawing.Point(686, 13);
            this.RegistersGroupBox.Name = "RegistersGroupBox";
            this.RegistersGroupBox.Size = new System.Drawing.Size(286, 110);
            this.RegistersGroupBox.TabIndex = 65;
            this.RegistersGroupBox.TabStop = false;
            this.RegistersGroupBox.Text = "Registers";
            // 
            // CCTextBox
            // 
            this.CCTextBox.Location = new System.Drawing.Point(181, 16);
            this.CCTextBox.Name = "CCTextBox";
            this.CCTextBox.Size = new System.Drawing.Size(15, 20);
            this.CCTextBox.TabIndex = 8;
            this.CCTextBox.Text = "Z";
            this.CCTextBox.TextChanged += new System.EventHandler(this.CCTextBox_TextChanged);
            this.CCTextBox.Leave += new System.EventHandler(this.CCTextBox_Leave);
            // 
            // CCLabel
            // 
            this.CCLabel.AutoSize = true;
            this.CCLabel.Location = new System.Drawing.Point(154, 20);
            this.CCLabel.Name = "CCLabel";
            this.CCLabel.Size = new System.Drawing.Size(21, 13);
            this.CCLabel.TabIndex = 24;
            this.CCLabel.Text = "CC";
            // 
            // MDRTextBox
            // 
            this.MDRTextBox.Location = new System.Drawing.Point(246, 82);
            this.MDRTextBox.MaxLength = 4;
            this.MDRTextBox.Name = "MDRTextBox";
            this.MDRTextBox.Size = new System.Drawing.Size(34, 20);
            this.MDRTextBox.TabIndex = 12;
            this.MDRTextBox.Text = "0003";
            this.MDRTextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.MDRTextBox.Leave += new System.EventHandler(this.MDRTextBox_Leave);
            // 
            // MDRLabel
            // 
            this.MDRLabel.AutoSize = true;
            this.MDRLabel.Location = new System.Drawing.Point(208, 85);
            this.MDRLabel.Name = "MDRLabel";
            this.MDRLabel.Size = new System.Drawing.Size(32, 13);
            this.MDRLabel.TabIndex = 22;
            this.MDRLabel.Text = "MDR";
            // 
            // MARTextBox
            // 
            this.MARTextBox.Location = new System.Drawing.Point(246, 60);
            this.MARTextBox.MaxLength = 4;
            this.MARTextBox.Name = "MARTextBox";
            this.MARTextBox.Size = new System.Drawing.Size(34, 20);
            this.MARTextBox.TabIndex = 11;
            this.MARTextBox.Text = "0002";
            this.MARTextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.MARTextBox.Leave += new System.EventHandler(this.MARTextBox_Leave);
            // 
            // MARLabel
            // 
            this.MARLabel.AutoSize = true;
            this.MARLabel.Location = new System.Drawing.Point(209, 62);
            this.MARLabel.Name = "MARLabel";
            this.MARLabel.Size = new System.Drawing.Size(31, 13);
            this.MARLabel.TabIndex = 20;
            this.MARLabel.Text = "MAR";
            // 
            // IRTextBox
            // 
            this.IRTextBox.Location = new System.Drawing.Point(246, 39);
            this.IRTextBox.MaxLength = 4;
            this.IRTextBox.Name = "IRTextBox";
            this.IRTextBox.Size = new System.Drawing.Size(34, 20);
            this.IRTextBox.TabIndex = 10;
            this.IRTextBox.Text = "0001";
            this.IRTextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.IRTextBox.Leave += new System.EventHandler(this.IRTextBox_Leave);
            // 
            // IRLabel
            // 
            this.IRLabel.AutoSize = true;
            this.IRLabel.Location = new System.Drawing.Point(222, 42);
            this.IRLabel.Name = "IRLabel";
            this.IRLabel.Size = new System.Drawing.Size(18, 13);
            this.IRLabel.TabIndex = 18;
            this.IRLabel.Text = "IR";
            // 
            // PCTextBox
            // 
            this.PCTextBox.Location = new System.Drawing.Point(246, 17);
            this.PCTextBox.MaxLength = 4;
            this.PCTextBox.Name = "PCTextBox";
            this.PCTextBox.Size = new System.Drawing.Size(34, 20);
            this.PCTextBox.TabIndex = 9;
            this.PCTextBox.Text = "0000";
            this.PCTextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.PCTextBox.Leave += new System.EventHandler(this.PCTextBox_Leave);
            // 
            // PCLabel
            // 
            this.PCLabel.AutoSize = true;
            this.PCLabel.Location = new System.Drawing.Point(219, 21);
            this.PCLabel.Name = "PCLabel";
            this.PCLabel.Size = new System.Drawing.Size(21, 13);
            this.PCLabel.TabIndex = 16;
            this.PCLabel.Text = "PC";
            // 
            // R7TextBox
            // 
            this.R7TextBox.Location = new System.Drawing.Point(104, 82);
            this.R7TextBox.MaxLength = 4;
            this.R7TextBox.Name = "R7TextBox";
            this.R7TextBox.Size = new System.Drawing.Size(34, 20);
            this.R7TextBox.TabIndex = 7;
            this.R7TextBox.Text = "FFF8";
            this.R7TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R7TextBox.Leave += new System.EventHandler(this.R7TextBox_Leave);
            // 
            // R7Label
            // 
            this.R7Label.AutoSize = true;
            this.R7Label.Location = new System.Drawing.Point(77, 85);
            this.R7Label.Name = "R7Label";
            this.R7Label.Size = new System.Drawing.Size(21, 13);
            this.R7Label.TabIndex = 14;
            this.R7Label.Text = "R7";
            // 
            // R6TextBox
            // 
            this.R6TextBox.Location = new System.Drawing.Point(104, 60);
            this.R6TextBox.MaxLength = 4;
            this.R6TextBox.Name = "R6TextBox";
            this.R6TextBox.Size = new System.Drawing.Size(34, 20);
            this.R6TextBox.TabIndex = 6;
            this.R6TextBox.Text = "FFF9";
            this.R6TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R6TextBox.Leave += new System.EventHandler(this.R6TextBox_Leave);
            // 
            // R6Label
            // 
            this.R6Label.AutoSize = true;
            this.R6Label.Location = new System.Drawing.Point(77, 63);
            this.R6Label.Name = "R6Label";
            this.R6Label.Size = new System.Drawing.Size(21, 13);
            this.R6Label.TabIndex = 12;
            this.R6Label.Text = "R6";
            // 
            // R5TextBox
            // 
            this.R5TextBox.Location = new System.Drawing.Point(104, 39);
            this.R5TextBox.MaxLength = 4;
            this.R5TextBox.Name = "R5TextBox";
            this.R5TextBox.Size = new System.Drawing.Size(34, 20);
            this.R5TextBox.TabIndex = 5;
            this.R5TextBox.Text = "FFFA";
            this.R5TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R5TextBox.Leave += new System.EventHandler(this.R5TextBox_Leave);
            // 
            // R5Label
            // 
            this.R5Label.AutoSize = true;
            this.R5Label.Location = new System.Drawing.Point(77, 42);
            this.R5Label.Name = "R5Label";
            this.R5Label.Size = new System.Drawing.Size(21, 13);
            this.R5Label.TabIndex = 10;
            this.R5Label.Text = "R5";
            // 
            // R4TextBox
            // 
            this.R4TextBox.Location = new System.Drawing.Point(104, 17);
            this.R4TextBox.MaxLength = 4;
            this.R4TextBox.Name = "R4TextBox";
            this.R4TextBox.Size = new System.Drawing.Size(34, 20);
            this.R4TextBox.TabIndex = 4;
            this.R4TextBox.Text = "FFFB";
            this.R4TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R4TextBox.Leave += new System.EventHandler(this.R4TextBox_Leave);
            // 
            // R4Label
            // 
            this.R4Label.AutoSize = true;
            this.R4Label.Location = new System.Drawing.Point(77, 20);
            this.R4Label.Name = "R4Label";
            this.R4Label.Size = new System.Drawing.Size(21, 13);
            this.R4Label.TabIndex = 8;
            this.R4Label.Text = "R4";
            // 
            // R3TextBox
            // 
            this.R3TextBox.Location = new System.Drawing.Point(34, 82);
            this.R3TextBox.MaxLength = 4;
            this.R3TextBox.Name = "R3TextBox";
            this.R3TextBox.Size = new System.Drawing.Size(34, 20);
            this.R3TextBox.TabIndex = 3;
            this.R3TextBox.Text = "FFFC";
            this.R3TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R3TextBox.Leave += new System.EventHandler(this.R3TextBox_Leave);
            // 
            // R3Label
            // 
            this.R3Label.AutoSize = true;
            this.R3Label.Location = new System.Drawing.Point(7, 85);
            this.R3Label.Name = "R3Label";
            this.R3Label.Size = new System.Drawing.Size(21, 13);
            this.R3Label.TabIndex = 6;
            this.R3Label.Text = "R3";
            // 
            // R2TextBox
            // 
            this.R2TextBox.Location = new System.Drawing.Point(34, 60);
            this.R2TextBox.MaxLength = 4;
            this.R2TextBox.Name = "R2TextBox";
            this.R2TextBox.Size = new System.Drawing.Size(34, 20);
            this.R2TextBox.TabIndex = 2;
            this.R2TextBox.Text = "FFFD";
            this.R2TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R2TextBox.Leave += new System.EventHandler(this.R2TextBox_Leave);
            // 
            // R2Label
            // 
            this.R2Label.AutoSize = true;
            this.R2Label.Location = new System.Drawing.Point(7, 63);
            this.R2Label.Name = "R2Label";
            this.R2Label.Size = new System.Drawing.Size(21, 13);
            this.R2Label.TabIndex = 4;
            this.R2Label.Text = "R2";
            // 
            // R1TextBox
            // 
            this.R1TextBox.Location = new System.Drawing.Point(34, 39);
            this.R1TextBox.MaxLength = 4;
            this.R1TextBox.Name = "R1TextBox";
            this.R1TextBox.Size = new System.Drawing.Size(34, 20);
            this.R1TextBox.TabIndex = 1;
            this.R1TextBox.Text = "FFFE";
            this.R1TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R1TextBox.Leave += new System.EventHandler(this.R1TextBox_Leave);
            // 
            // R1Label
            // 
            this.R1Label.AutoSize = true;
            this.R1Label.Location = new System.Drawing.Point(7, 42);
            this.R1Label.Name = "R1Label";
            this.R1Label.Size = new System.Drawing.Size(21, 13);
            this.R1Label.TabIndex = 2;
            this.R1Label.Text = "R1";
            // 
            // R0TextBox
            // 
            this.R0TextBox.Location = new System.Drawing.Point(34, 17);
            this.R0TextBox.MaxLength = 4;
            this.R0TextBox.Name = "R0TextBox";
            this.R0TextBox.Size = new System.Drawing.Size(34, 20);
            this.R0TextBox.TabIndex = 0;
            this.R0TextBox.Text = "FFFF";
            this.R0TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.R0TextBox.Leave += new System.EventHandler(this.R0TextBox_Leave);
            // 
            // R0Label
            // 
            this.R0Label.AutoSize = true;
            this.R0Label.Location = new System.Drawing.Point(7, 20);
            this.R0Label.Name = "R0Label";
            this.R0Label.Size = new System.Drawing.Size(21, 13);
            this.R0Label.TabIndex = 0;
            this.R0Label.Text = "R0";
            // 
            // MemoryGroupBox
            // 
            this.MemoryGroupBox.Controls.Add(this.MemoryContents3TextBox);
            this.MemoryGroupBox.Controls.Add(this.MemoryContents2TextBox);
            this.MemoryGroupBox.Controls.Add(this.MemoryContents1TextBox);
            this.MemoryGroupBox.Controls.Add(this.MemoryContents0TextBox);
            this.MemoryGroupBox.Controls.Add(this.MemoryAddr3Label);
            this.MemoryGroupBox.Controls.Add(this.MemoryAddr2Label);
            this.MemoryGroupBox.Controls.Add(this.MemoryAddr1Label);
            this.MemoryGroupBox.Controls.Add(this.MemoryAddr0TextBox);
            this.MemoryGroupBox.Location = new System.Drawing.Point(685, 129);
            this.MemoryGroupBox.Name = "MemoryGroupBox";
            this.MemoryGroupBox.Size = new System.Drawing.Size(287, 117);
            this.MemoryGroupBox.TabIndex = 66;
            this.MemoryGroupBox.TabStop = false;
            this.MemoryGroupBox.Text = "Memory";
            // 
            // MemoryContents3TextBox
            // 
            this.MemoryContents3TextBox.Location = new System.Drawing.Point(48, 89);
            this.MemoryContents3TextBox.MaxLength = 4;
            this.MemoryContents3TextBox.Name = "MemoryContents3TextBox";
            this.MemoryContents3TextBox.Size = new System.Drawing.Size(38, 20);
            this.MemoryContents3TextBox.TabIndex = 3;
            this.MemoryContents3TextBox.Text = "6666";
            this.MemoryContents3TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.MemoryContents3TextBox.Leave += new System.EventHandler(this.MemoryContents3TextBox_Leave);
            // 
            // MemoryContents2TextBox
            // 
            this.MemoryContents2TextBox.Location = new System.Drawing.Point(48, 66);
            this.MemoryContents2TextBox.MaxLength = 4;
            this.MemoryContents2TextBox.Name = "MemoryContents2TextBox";
            this.MemoryContents2TextBox.Size = new System.Drawing.Size(38, 20);
            this.MemoryContents2TextBox.TabIndex = 2;
            this.MemoryContents2TextBox.Text = "7777";
            this.MemoryContents2TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.MemoryContents2TextBox.Leave += new System.EventHandler(this.MemoryContents2TextBox_Leave);
            // 
            // MemoryContents1TextBox
            // 
            this.MemoryContents1TextBox.Location = new System.Drawing.Point(48, 43);
            this.MemoryContents1TextBox.MaxLength = 4;
            this.MemoryContents1TextBox.Name = "MemoryContents1TextBox";
            this.MemoryContents1TextBox.Size = new System.Drawing.Size(38, 20);
            this.MemoryContents1TextBox.TabIndex = 1;
            this.MemoryContents1TextBox.Text = "8888";
            this.MemoryContents1TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.MemoryContents1TextBox.Leave += new System.EventHandler(this.MemoryContents1TextBox_Leave);
            // 
            // MemoryContents0TextBox
            // 
            this.MemoryContents0TextBox.Location = new System.Drawing.Point(48, 20);
            this.MemoryContents0TextBox.MaxLength = 4;
            this.MemoryContents0TextBox.Name = "MemoryContents0TextBox";
            this.MemoryContents0TextBox.Size = new System.Drawing.Size(38, 20);
            this.MemoryContents0TextBox.TabIndex = 0;
            this.MemoryContents0TextBox.Text = "9999";
            this.MemoryContents0TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.MemoryContents0TextBox.Leave += new System.EventHandler(this.MemoryContents0TextBox_Leave);
            // 
            // MemoryAddr3Label
            // 
            this.MemoryAddr3Label.AutoSize = true;
            this.MemoryAddr3Label.Location = new System.Drawing.Point(8, 92);
            this.MemoryAddr3Label.Name = "MemoryAddr3Label";
            this.MemoryAddr3Label.Size = new System.Drawing.Size(31, 13);
            this.MemoryAddr3Label.TabIndex = 3;
            this.MemoryAddr3Label.Text = "F0F3";
            // 
            // MemoryAddr2Label
            // 
            this.MemoryAddr2Label.AutoSize = true;
            this.MemoryAddr2Label.Location = new System.Drawing.Point(8, 69);
            this.MemoryAddr2Label.Name = "MemoryAddr2Label";
            this.MemoryAddr2Label.Size = new System.Drawing.Size(31, 13);
            this.MemoryAddr2Label.TabIndex = 2;
            this.MemoryAddr2Label.Text = "F0F2";
            // 
            // MemoryAddr1Label
            // 
            this.MemoryAddr1Label.AutoSize = true;
            this.MemoryAddr1Label.Location = new System.Drawing.Point(8, 46);
            this.MemoryAddr1Label.Name = "MemoryAddr1Label";
            this.MemoryAddr1Label.Size = new System.Drawing.Size(31, 13);
            this.MemoryAddr1Label.TabIndex = 1;
            this.MemoryAddr1Label.Text = "F0F1";
            // 
            // MemoryAddr0TextBox
            // 
            this.MemoryAddr0TextBox.Location = new System.Drawing.Point(7, 20);
            this.MemoryAddr0TextBox.MaxLength = 4;
            this.MemoryAddr0TextBox.Name = "MemoryAddr0TextBox";
            this.MemoryAddr0TextBox.Size = new System.Drawing.Size(35, 20);
            this.MemoryAddr0TextBox.TabIndex = 0;
            this.MemoryAddr0TextBox.Text = "F0F0";
            this.MemoryAddr0TextBox.TextChanged += new System.EventHandler(this.RestrictTextBoxTo16BitHexInput_TextChanged);
            this.MemoryAddr0TextBox.Leave += new System.EventHandler(this.MemoryAddr0TextBox_Leave);
            // 
            // RandomizeMachineButton
            // 
            this.RandomizeMachineButton.Location = new System.Drawing.Point(692, 525);
            this.RandomizeMachineButton.Name = "RandomizeMachineButton";
            this.RandomizeMachineButton.Size = new System.Drawing.Size(132, 23);
            this.RandomizeMachineButton.TabIndex = 67;
            this.RandomizeMachineButton.Text = "Randomize Machine";
            this.RandomizeMachineButton.UseVisualStyleBackColor = true;
            this.RandomizeMachineButton.Click += new System.EventHandler(this.RandomizeMachineButton_Click);
            // 
            // ZeroMachineButton
            // 
            this.ZeroMachineButton.Location = new System.Drawing.Point(692, 554);
            this.ZeroMachineButton.Name = "ZeroMachineButton";
            this.ZeroMachineButton.Size = new System.Drawing.Size(132, 23);
            this.ZeroMachineButton.TabIndex = 68;
            this.ZeroMachineButton.Text = "Zero Machine";
            this.ZeroMachineButton.UseVisualStyleBackColor = true;
            this.ZeroMachineButton.Click += new System.EventHandler(this.ZeroMachineButton_Click);
            // 
            // ControlSignalInterpretationGroupBox
            // 
            this.ControlSignalInterpretationGroupBox.Controls.Add(this.label1);
            this.ControlSignalInterpretationGroupBox.Location = new System.Drawing.Point(686, 253);
            this.ControlSignalInterpretationGroupBox.Name = "ControlSignalInterpretationGroupBox";
            this.ControlSignalInterpretationGroupBox.Size = new System.Drawing.Size(286, 254);
            this.ControlSignalInterpretationGroupBox.TabIndex = 69;
            this.ControlSignalInterpretationGroupBox.TabStop = false;
            this.ControlSignalInterpretationGroupBox.Text = "Control Signal Interpretation";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.MaximumSize = new System.Drawing.Size(275, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 182);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // dataPathPictureBox
            // 
            this.dataPathPictureBox.Image = global::LC_3ControlSignalExplorer.Properties.Resources.data_path;
            this.dataPathPictureBox.Location = new System.Drawing.Point(0, 0);
            this.dataPathPictureBox.Name = "dataPathPictureBox";
            this.dataPathPictureBox.Size = new System.Drawing.Size(544, 700);
            this.dataPathPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.dataPathPictureBox.TabIndex = 0;
            this.dataPathPictureBox.TabStop = false;
            // 
            // IconAttributionLinkLabel
            // 
            this.IconAttributionLinkLabel.AutoSize = true;
            this.IconAttributionLinkLabel.Location = new System.Drawing.Point(745, 687);
            this.IconAttributionLinkLabel.Name = "IconAttributionLinkLabel";
            this.IconAttributionLinkLabel.Size = new System.Drawing.Size(228, 13);
            this.IconAttributionLinkLabel.TabIndex = 70;
            this.IconAttributionLinkLabel.TabStop = true;
            this.IconAttributionLinkLabel.Text = "Icon based on photo by torbakhopper on Flickr";
            this.IconAttributionLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.IconAttributionLinkLabel_LinkClicked);
            // 
            // LC3ControlSignalExplorerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 711);
            this.Controls.Add(this.IconAttributionLinkLabel);
            this.Controls.Add(this.ControlSignalInterpretationGroupBox);
            this.Controls.Add(this.ZeroMachineButton);
            this.Controls.Add(this.RandomizeMachineButton);
            this.Controls.Add(this.MemoryGroupBox);
            this.Controls.Add(this.RegistersGroupBox);
            this.Controls.Add(this.ControlSignalsGroupBox);
            this.Controls.Add(this.WaitForMemoryButton);
            this.Controls.Add(this.WaitOneCycleButton);
            this.Controls.Add(this.MDROutputWireLabel);
            this.Controls.Add(this.PCMUXOutputCanBeErrorLabel);
            this.Controls.Add(this.PCIncWireLabel);
            this.Controls.Add(this.MARMUXOutputLabel);
            this.Controls.Add(this.PCOutputWireLabel);
            this.Controls.Add(this.ADDRAdderOutputLabel);
            this.Controls.Add(this.ADDR1MUXOutputLabel);
            this.Controls.Add(this.ADDR2MUXOutputWireLabel);
            this.Controls.Add(this.ADDR2MUXInput2Label);
            this.Controls.Add(this.ADDR2MUXInput1Label);
            this.Controls.Add(this.ADDR2MUXInput0Label);
            this.Controls.Add(this.SystemBusCanBeErrorLabel);
            this.Controls.Add(this.ALUOutputWireLabel);
            this.Controls.Add(this.SR2MUXOutputWireLabel);
            this.Controls.Add(this.SR2MUXInput0WireLabel);
            this.Controls.Add(this.IROutWireLabel);
            this.Controls.Add(this.SR1OutWireLabel);
            this.Controls.Add(this.SR2OutWireLabel);
            this.Controls.Add(this.MARMUXInput0Label);
            this.Controls.Add(this.dataPathPictureBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LC3ControlSignalExplorerForm";
            this.Text = "LC-3 Control Signal Explorer";
            this.ControlSignalsGroupBox.ResumeLayout(false);
            this.ControlSignalsGroupBox.PerformLayout();
            this.RegistersGroupBox.ResumeLayout(false);
            this.RegistersGroupBox.PerformLayout();
            this.MemoryGroupBox.ResumeLayout(false);
            this.MemoryGroupBox.PerformLayout();
            this.ControlSignalInterpretationGroupBox.ResumeLayout(false);
            this.ControlSignalInterpretationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataPathPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox dataPathPictureBox;
        private System.Windows.Forms.MaskedTextBox GateMARMUXTextBox;
        private System.Windows.Forms.Label GateMARMUXLabel;
        private System.Windows.Forms.Label GatePCLabel;
        private System.Windows.Forms.MaskedTextBox GatePCTextBox;
        private System.Windows.Forms.Label MARMUXLabel;
        private System.Windows.Forms.MaskedTextBox MARMUXTextBox;
        private System.Windows.Forms.Label LD_PCLabel;
        private System.Windows.Forms.MaskedTextBox LD_PCTextBox;
        private System.Windows.Forms.Label DRLabel;
        private System.Windows.Forms.MaskedTextBox DRTextBox;
        private System.Windows.Forms.Label PCMUXLabel;
        private System.Windows.Forms.MaskedTextBox PCMUXTextBox;
        private System.Windows.Forms.Label LD_REGLabel;
        private System.Windows.Forms.MaskedTextBox LD_REGTextBox;
        private System.Windows.Forms.Label SR1Label;
        private System.Windows.Forms.MaskedTextBox SR1TextBox;
        private System.Windows.Forms.Label SR2Label;
        private System.Windows.Forms.MaskedTextBox SR2TextBox;
        private System.Windows.Forms.Label ADDR1MUXLabel;
        private System.Windows.Forms.MaskedTextBox ADDR1MUXTextBox;
        private System.Windows.Forms.Label ADDR2MUXLabel;
        private System.Windows.Forms.MaskedTextBox ADDR2MUXTextBox;
        private System.Windows.Forms.Label SR2MUXLabel;
        private System.Windows.Forms.MaskedTextBox SR2MUXTextBox;
        private System.Windows.Forms.Label ALUKLabel;
        private System.Windows.Forms.MaskedTextBox ALUKTextBox;
        private System.Windows.Forms.Label LD_IRLabel;
        private System.Windows.Forms.MaskedTextBox LD_IRTextBox;
        private System.Windows.Forms.Label LD_CCLabel;
        private System.Windows.Forms.MaskedTextBox LD_CCTextBox;
        private System.Windows.Forms.Label GateALULabel;
        private System.Windows.Forms.MaskedTextBox GateALUTextBox;
        private System.Windows.Forms.Label GateMDRLabel;
        private System.Windows.Forms.MaskedTextBox GateMDRTextBox;
        private System.Windows.Forms.Label LD_MDRLabel;
        private System.Windows.Forms.MaskedTextBox LD_MDRTextBox;
        private System.Windows.Forms.Label LD_MARLabel;
        private System.Windows.Forms.MaskedTextBox LD_MARTextBox;
        private System.Windows.Forms.Label MEM_ENLabel;
        private System.Windows.Forms.MaskedTextBox MEM_ENTextBox;
        private System.Windows.Forms.Label R_WLabel;
        private System.Windows.Forms.MaskedTextBox R_WTextBox;
        private System.Windows.Forms.Label MARMUXInput0Label;
        private System.Windows.Forms.Label SR2OutWireLabel;
        private System.Windows.Forms.Label SR1OutWireLabel;
        private System.Windows.Forms.Label IROutWireLabel;
        private System.Windows.Forms.Label SR2MUXInput0WireLabel;
        private System.Windows.Forms.Label SR2MUXOutputWireLabel;
        private System.Windows.Forms.Label ALUOutputWireLabel;
        private System.Windows.Forms.Label SystemBusCanBeErrorLabel;
        private System.Windows.Forms.Label ADDR2MUXInput0Label;
        private System.Windows.Forms.Label ADDR2MUXInput1Label;
        private System.Windows.Forms.Label ADDR2MUXInput2Label;
        private System.Windows.Forms.Label ADDR2MUXOutputWireLabel;
        private System.Windows.Forms.Label ADDR1MUXOutputLabel;
        private System.Windows.Forms.Label ADDRAdderOutputLabel;
        private System.Windows.Forms.Label PCOutputWireLabel;
        private System.Windows.Forms.Label MARMUXOutputLabel;
        private System.Windows.Forms.Label PCIncWireLabel;
        private System.Windows.Forms.Label PCMUXOutputCanBeErrorLabel;
        private System.Windows.Forms.Label MDROutputWireLabel;
        private System.Windows.Forms.Button WaitOneCycleButton;
        private System.Windows.Forms.Button WaitForMemoryButton;
        private System.Windows.Forms.GroupBox ControlSignalsGroupBox;
        private System.Windows.Forms.GroupBox RegistersGroupBox;
        private System.Windows.Forms.TextBox R3TextBox;
        private System.Windows.Forms.Label R3Label;
        private System.Windows.Forms.TextBox R2TextBox;
        private System.Windows.Forms.Label R2Label;
        private System.Windows.Forms.TextBox R1TextBox;
        private System.Windows.Forms.Label R1Label;
        private System.Windows.Forms.TextBox R0TextBox;
        private System.Windows.Forms.Label R0Label;
        private System.Windows.Forms.TextBox R7TextBox;
        private System.Windows.Forms.Label R7Label;
        private System.Windows.Forms.TextBox R6TextBox;
        private System.Windows.Forms.Label R6Label;
        private System.Windows.Forms.TextBox R5TextBox;
        private System.Windows.Forms.Label R5Label;
        private System.Windows.Forms.TextBox R4TextBox;
        private System.Windows.Forms.Label R4Label;
        private System.Windows.Forms.TextBox MDRTextBox;
        private System.Windows.Forms.Label MDRLabel;
        private System.Windows.Forms.TextBox MARTextBox;
        private System.Windows.Forms.Label MARLabel;
        private System.Windows.Forms.TextBox IRTextBox;
        private System.Windows.Forms.Label IRLabel;
        private System.Windows.Forms.TextBox PCTextBox;
        private System.Windows.Forms.Label PCLabel;
        private System.Windows.Forms.TextBox CCTextBox;
        private System.Windows.Forms.Label CCLabel;
        private System.Windows.Forms.GroupBox MemoryGroupBox;
        private System.Windows.Forms.TextBox MemoryContents3TextBox;
        private System.Windows.Forms.TextBox MemoryContents2TextBox;
        private System.Windows.Forms.TextBox MemoryContents1TextBox;
        private System.Windows.Forms.TextBox MemoryContents0TextBox;
        private System.Windows.Forms.Label MemoryAddr3Label;
        private System.Windows.Forms.Label MemoryAddr2Label;
        private System.Windows.Forms.Label MemoryAddr1Label;
        private System.Windows.Forms.TextBox MemoryAddr0TextBox;
        private System.Windows.Forms.Button RandomizeMachineButton;
        private System.Windows.Forms.Button ZeroMachineButton;
        private System.Windows.Forms.GroupBox ControlSignalInterpretationGroupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel IconAttributionLinkLabel;
    }
}

