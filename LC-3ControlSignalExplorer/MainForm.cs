﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LC_3ControlSignalExplorer
{
    public partial class LC3ControlSignalExplorerForm : Form
    {
        /// <summary>
        /// The LC-3 system viewed and modified by this form
        /// </summary>
        LC3 lc3 = new LC3();

        public LC3ControlSignalExplorerForm()
        {
            InitializeComponent();
            InitControlSignalTextBoxes();
            UpdateGUI();
        }

        /// <summary>
        /// Initializes the text boxes to match the current values of their corresponding control signals
        /// </summary>
        private void InitControlSignalTextBoxes()
        {
            ADDR1MUXTextBox.Text = lc3.ADDR1MUX.asString;
            ADDR2MUXTextBox.Text = lc3.ADDR2MUX.asString;
            ALUKTextBox.Text = lc3.ALUK.asString;
            DRTextBox.Text = lc3.DR.asString;
            GateALUTextBox.Text = lc3.GateALU.asString;
            GateMARMUXTextBox.Text = lc3.GateMARMUX.asString;
            GateMDRTextBox.Text = lc3.GateMDR.asString;
            GatePCTextBox.Text = lc3.GatePC.asString;
            LD_CCTextBox.Text = lc3.LD_CC.asString;
            LD_IRTextBox.Text = lc3.LD_IR.asString;
            LD_MARTextBox.Text = lc3.LD_MAR.asString;
            LD_MDRTextBox.Text = lc3.LD_MDR.asString;
            LD_PCTextBox.Text = lc3.LD_PC.asString;
            LD_REGTextBox.Text = lc3.LD_REG.asString;
            MARMUXTextBox.Text = lc3.MARMUX.asString;
            MEM_ENTextBox.Text = lc3.MEM_EN.asString;
            PCMUXTextBox.Text = lc3.PCMUX.asString;
            R_WTextBox.Text = lc3.R_W.asString;
            SR1TextBox.Text = lc3.SR1.asString;
            SR2TextBox.Text = lc3.SR2.asString;
            SR2MUXTextBox.Text = lc3.SR2MUX.asString;
        }

        /// <summary>
        /// Update the different GUI components that view parts of the LC-3
        /// </summary>
        private void UpdateGUI()
        {
            //Set Wire/Bus Displays
            SetLabelFromSignal(MARMUXInput0Label, lc3.MARMUXInput0());
            SetLabelFromSignal(SR1OutWireLabel, lc3.SR1OutWire());
            SetLabelFromSignal(SR2OutWireLabel, lc3.SR2OutWire());
            SetLabelFromSignal(IROutWireLabel, lc3.IROutWire());
            SetLabelFromSignal(SR2MUXInput0WireLabel, lc3.SR2MUXInput0Wire());
            SetLabelFromSignal(SR2MUXOutputWireLabel, lc3.SR2MUXOutputWire());
            SetLabelFromSignal(ALUOutputWireLabel, lc3.ALUOutputWire());

            SetLabelFromSignal(ADDR2MUXInput0Label, lc3.ADDR2MUXInput0());
            SetLabelFromSignal(ADDR2MUXInput1Label, lc3.ADDR2MUXInput1());
            SetLabelFromSignal(ADDR2MUXInput2Label, lc3.ADDR2MUXInput2());
            SetLabelFromSignal(ADDR2MUXOutputWireLabel, lc3.ADDR2MUXOutputWire());

            SetLabelFromSignal(PCOutputWireLabel, lc3.PCOutputWire());
            SetLabelFromSignal(ADDR1MUXOutputLabel, lc3.ADDR1MUXOutput());
            SetLabelFromSignal(ADDRAdderOutputLabel, lc3.ADDRAdderOutput());
            SetLabelFromSignal(MARMUXInput0Label, lc3.MARMUXInput0());
            SetLabelFromSignal(MARMUXOutputLabel, lc3.MARMUXOutput());

            SetLabelFromSignal(PCIncWireLabel, lc3.PCIncWire());
            SetLabelFromSignal(MDROutputWireLabel, lc3.MDROutputWire());

            SetLabelFromSignal(SystemBusCanBeErrorLabel, lc3.SystemBusCanBeError());

            SetLabelFromSignal(PCMUXOutputCanBeErrorLabel, lc3.PCMUXOutputCanBeError());

            // Set General Purpose Register Display
            SetHexTextBoxFromUInt16(R0TextBox, lc3.registers[0]);
            SetHexTextBoxFromUInt16(R1TextBox, lc3.registers[1]);
            SetHexTextBoxFromUInt16(R2TextBox, lc3.registers[2]);
            SetHexTextBoxFromUInt16(R3TextBox, lc3.registers[3]);
            SetHexTextBoxFromUInt16(R4TextBox, lc3.registers[4]);
            SetHexTextBoxFromUInt16(R5TextBox, lc3.registers[5]);
            SetHexTextBoxFromUInt16(R6TextBox, lc3.registers[6]);
            SetHexTextBoxFromUInt16(R7TextBox, lc3.registers[7]);

            // Set Special Purpose Hex Register Display
            SetHexTextBoxFromUInt16(PCTextBox, lc3.PC);
            SetHexTextBoxFromUInt16(IRTextBox, lc3.IR);
            SetHexTextBoxFromUInt16(MARTextBox, lc3.MAR);
            SetHexTextBoxFromUInt16(MDRTextBox, lc3.MDR);

            // Set Memory Display
            UInt16 startAddr;
            try { startAddr = Convert.ToUInt16(MemoryAddr0TextBox.Text, 16);
            } catch (Exception) { startAddr = 0; }

            SetHexLabelFromUInt16(MemoryAddr1Label, (UInt16)((startAddr + 1) & 0xFFFF));
            SetHexLabelFromUInt16(MemoryAddr2Label, (UInt16)((startAddr + 2) & 0xFFFF));
            SetHexLabelFromUInt16(MemoryAddr3Label, (UInt16)((startAddr + 3) & 0xFFFF));

            SetHexTextBoxFromUInt16(MemoryContents0TextBox, lc3.memory[startAddr + 0]);
            SetHexTextBoxFromUInt16(MemoryContents1TextBox, lc3.memory[startAddr + 1]);
            SetHexTextBoxFromUInt16(MemoryContents2TextBox, lc3.memory[startAddr + 2]);
            SetHexTextBoxFromUInt16(MemoryContents3TextBox, lc3.memory[startAddr + 3]);

            // Set CC Display
            CCTextBox.Text = new String(lc3.CC,1);
        }

        private void SetHexTextBoxFromUInt16(TextBox textBox, UInt16 val)
        {
            textBox.Text = ((UInt32)val).ToString("X4");
        }

        private void SetHexLabelFromUInt16(Label label, UInt16 val)
        {
            label.Text = ((UInt32)val).ToString("X4");
        }

        /// <summary>
        /// Sets the text and color of the label using the control signal string s
        /// </summary>
        /// <param name="l"></param>
        /// <param name="s">A string returned from one of the LC-3 Control signal functions like <code>SystemBusCanBeError()</code></param>
        private void SetLabelFromSignal(Label l, String s)
        {
            try
            {
                String hex = ((Int32)Convert.ToUInt16(s, 2)).ToString("X4");
                if (l.Text.Equals(hex))
                {
                    l.ForeColor = Color.Blue;
                }
                else
                {
                    l.Text = hex;
                    l.ForeColor = Color.Green;
                }
            }catch(FormatException)
            {
                String reflowed = s;
                if (reflowed.Length > 10)
                {
                    reflowed = s.Replace(' ', '\n');
                }
                l.Text = reflowed;
                l.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Used to avoid infinite recusion when changing text for a <code>TextChanged</code> inside its <code>TextChanged</code> event. It is used directly in the handlers and also by subroutines they call.
        /// 
        /// Note that this only works because the GUI is single-threaded.
        /// </summary>
        bool alreadyInsideTextChangedHandler = false;

        /// <summary>
        /// The random number generator used for randomizing the machine and possibly other sundry uses.
        /// </summary>
        static private Random random = new Random();

        /// <summary>
        /// Sets the given control signal <code>signal</code> based on the text in <code>input</code>. Then updates the GUI to reflect any changes.
        /// </summary>
        /// <param name="signal">The control signal - must be a member of <code>this.lc3</code></param>
        /// <param name="input">The text box containing the bits for the control signal. During execution, any non-'0' characters in the text are set to 1. These are the bits used to set the control signal.</param>
        void setControlSignal(ControlSignal signal, MaskedTextBox input)
        {
            forceCorrectInputForControlSignal(signal, input);
            if (input.Text.Length < signal.numBits)
            {
                input.Text = input.Text + new String('0', signal.numBits - input.Text.Length);
            }
            if (input.Text.Length > signal.numBits)
            {
                input.Text = input.Text.Substring(0, signal.numBits);
            }
            signal.asString = input.Text;
            UpdateGUI();
        }

        /// <summary>
        /// Forces <code>input</code> to have a binary integer format matching <code>signal</code> (but possibly missing bits)
        /// </summary>
        /// <param name="signal">The control signal</param>
        /// <param name="input">The text box whose input will be forced. During execution, any non-'0' characters in the text are set to 1.</param>
        void forceCorrectInputForControlSignal(ControlSignal signal, MaskedTextBox input)
        {
            if (!alreadyInsideTextChangedHandler)
            {
                alreadyInsideTextChangedHandler = true;
                Regex r = new Regex("[^0]");
                input.Text = r.Replace(input.Text, "1");
                alreadyInsideTextChangedHandler = false;
            }
        }

        /// <summary>
        /// Sets the value in <code>toSet</code> from the text in textBox assuming that textBox contains a 16 bit unsigned hex number. Then updates the GUI
        /// </summary>
        /// <param name="textbox">The TextBox containing the future value of set</param>
        /// <param name="toSet">The variable whose value will be set</param>
        private void SetUInt16FromHexTextBox(TextBox textbox, out UInt16 toSet)
        {
            try{
                toSet = Convert.ToUInt16(textbox.Text, 16);
            }catch(Exception){ 
                toSet = 0; 
            }

            UpdateGUI();
        }

        /// <summary>
        /// In the <code>lc3</code> member, set a memory location given by <code>startAddrTB</code>+offset to the value contained in <code>contentsTB</code> then update the GUI. All text boxes are assumed to contain 16 bit hex values.
        /// </summary>
        /// <param name="contentsTB">The value to be stored in the memory location (as a hex string)</param>
        /// <param name="startAddrTB">The address to be used if the offset is 0 (as a hex string)</param>
        /// <param name="offset">The offset to add to the <code>startAddrTB</code> to get the actual address where the value will be stored.</param>
        private void SetMemoryFromHexTextBox(TextBox contentsTB, TextBox startAddrTB, UInt16 offset)
        {
            UInt16 startAddr;
            try { 
                startAddr = Convert.ToUInt16(startAddrTB.Text, 16); 
            } catch (Exception) { startAddr = 0; 
            }

            UInt16 destAddr = (UInt16)((startAddr + offset) & 0xFFFF);
            SetUInt16FromHexTextBox(contentsTB, out lc3.memory[destAddr]);
        }

        /// <summary>
        /// A TextChagned event handler whose sender must be a TextBox that alters any input text so it has at most 4 characters and they are all valid hex digits (0-9,A-F)
        /// </summary>
        /// <param name="sender">A TextBox object</param>
        /// <param name="e">A TextChangedEvent</param>
        private void RestrictTextBoxTo16BitHexInput_TextChanged(object sender, EventArgs e)
        {
            TextBox input = (TextBox)sender;
            String newText;
            if (!alreadyInsideTextChangedHandler)
            {
                alreadyInsideTextChangedHandler = true;
                Regex r = new Regex("[^0-9A-Fa-f]");
                newText = r.Replace(input.Text, "F");
                if (newText.Length > 4)
                {
                    newText = newText.Substring(0, 4);
                }
                input.Text = newText;
                alreadyInsideTextChangedHandler = false;
            }
        }

        private void ADDR1MUXTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.ADDR1MUX, ADDR1MUXTextBox);
        }

        private void ADDR2MUXTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.ADDR2MUX, ADDR2MUXTextBox);
        }

        private void ALUKTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.ALUK, ALUKTextBox);
        }

        private void DRTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.DR, DRTextBox);
        }

        private void GateALUTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.GateALU, GateALUTextBox);
        }

        private void GateMARMUXTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.GateMARMUX, GateMARMUXTextBox);
        }

        private void GateMDRTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.GateMDR, GateMDRTextBox);
        }

        private void GatePCTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.GatePC, GatePCTextBox);
        }

        private void LD_CCTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.LD_CC, LD_CCTextBox);
        }

        private void LD_IRTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.LD_IR, LD_IRTextBox);
        }

        private void LD_MARTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.LD_MAR, LD_MARTextBox);
        }

        private void LD_MDRTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.LD_MDR, LD_MDRTextBox);
        }

        private void LD_PCTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.LD_PC, LD_PCTextBox);
        }

        private void LD_REGTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.LD_REG, LD_REGTextBox);
        }

        private void MARMUXTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.MARMUX, MARMUXTextBox);
        }

        private void MEM_ENTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.MEM_EN, MEM_ENTextBox);
        }

        private void PCMUXTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.PCMUX, PCMUXTextBox);
        }

        private void R_WTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.R_W, R_WTextBox);
        }

        private void SR1TextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.SR1, SR1TextBox);
        }

        private void SR2TextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.SR2, SR2TextBox);
        }

        private void SR2MUXTextBox_TextChanged(object sender, EventArgs e)
        {
            forceCorrectInputForControlSignal(lc3.SR2MUX, SR2MUXTextBox);
        }

        private void WaitOneCycleButton_Click(object sender, EventArgs e)
        {
            lc3.stepClock();
            UpdateGUI();
        }

        private void WaitForMemoryButton_Click(object sender, EventArgs e)
        {
            lc3.waitForMemoryToComplete();
            UpdateGUI();
        }

        private void RandomizeMachineButton_Click(object sender, EventArgs e)
        {
            lc3.randomizeMachine(random);
            UpdateGUI();
        }

        private void ZeroMachineButton_Click(object sender, EventArgs e)
        {
            lc3.zeroMachine();
            UpdateGUI();
        }

        private void R0TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R0TextBox, out lc3.registers[0]);
        }

        private void R1TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R1TextBox, out lc3.registers[1]);
        }

        private void R2TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R2TextBox, out lc3.registers[2]);
        }

        private void R3TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R3TextBox, out lc3.registers[3]);
        }

        private void R4TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R4TextBox, out lc3.registers[4]);
        }

        private void R5TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R5TextBox, out lc3.registers[5]);
        }

        private void R6TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R6TextBox, out lc3.registers[6]);
        }

        private void R7TextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox(R7TextBox, out lc3.registers[7]);
        }

        private void CCTextBox_Leave(object sender, EventArgs e)
        {
            lc3.CC = CCTextBox.Text[0];
            UpdateGUI();
        }

        private void PCTextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox((TextBox)sender, out lc3.PC);
        }

        private void IRTextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox((TextBox)sender, out lc3.IR);
        }

        private void MARTextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox((TextBox)sender, out lc3.MAR);
        }

        private void MDRTextBox_Leave(object sender, EventArgs e)
        {
            SetUInt16FromHexTextBox((TextBox)sender, out lc3.MDR);
        }

        private void MemoryAddr0TextBox_Leave(object sender, EventArgs e)
        {
            UpdateGUI();
        }

        private void MemoryContents0TextBox_Leave(object sender, EventArgs e)
        {
            SetMemoryFromHexTextBox((TextBox)sender, MemoryAddr0TextBox, 0);
        }

        private void MemoryContents1TextBox_Leave(object sender, EventArgs e)
        {
            SetMemoryFromHexTextBox((TextBox)sender, MemoryAddr0TextBox, 1);
        }

        private void MemoryContents2TextBox_Leave(object sender, EventArgs e)
        {
            SetMemoryFromHexTextBox((TextBox)sender, MemoryAddr0TextBox, 2);
        }

        private void MemoryContents3TextBox_Leave(object sender, EventArgs e)
        {
            SetMemoryFromHexTextBox((TextBox)sender, MemoryAddr0TextBox, 3);
        }

        private void CCTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!alreadyInsideTextChangedHandler)
            {
                alreadyInsideTextChangedHandler = true;

                TextBox input = (TextBox)sender;
                Regex r = new Regex("[^NZPnzp]");
                input.Text = r.Replace(input.Text, "Z");

                alreadyInsideTextChangedHandler = false;
            }
        }

        private void ADDR1MUXTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.ADDR1MUX, (MaskedTextBox)sender);
        }

        private void ADDR2MUXTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.ADDR2MUX, (MaskedTextBox)sender);
        }

        private void ALUKTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.ALUK, (MaskedTextBox)sender);
        }

        private void DRTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.DR, (MaskedTextBox)sender);
        }

        private void GateALUTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.GateALU, (MaskedTextBox)sender);
        }

        private void GateMARMUXTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.GateMARMUX, (MaskedTextBox)sender);
        }

        private void GateMDRTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.GateMDR, (MaskedTextBox)sender);
        }

        private void GatePCTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.GatePC, (MaskedTextBox)sender);
        }

        private void LD_CCTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.LD_CC, (MaskedTextBox)sender);
        }

        private void LD_IRTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.LD_IR, (MaskedTextBox)sender);
        }

        private void LD_MARTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.LD_MAR, (MaskedTextBox)sender);
        }

        private void LD_MDRTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.LD_MDR, (MaskedTextBox)sender);
        }

        private void LD_PCTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.LD_PC, (MaskedTextBox)sender);
        }

        private void LD_REGTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.LD_REG, (MaskedTextBox)sender);
        }

        private void MARMUXTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.MARMUX, (MaskedTextBox)sender);
        }

        private void MEM_ENTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.MEM_EN, (MaskedTextBox)sender);
        }

        private void PCMUXTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.PCMUX, (MaskedTextBox)sender);
        }

        private void R_WTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.R_W, (MaskedTextBox)sender);
        }

        private void SR1TextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.SR1, (MaskedTextBox)sender);
        }

        private void SR2TextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.SR2, (MaskedTextBox)sender);
        }

        private void SR2MUXTextBox_Leave(object sender, EventArgs e)
        {
            setControlSignal(lc3.SR2MUX, (MaskedTextBox)sender);
        }

        private void IconAttributionLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel ll = (LinkLabel)sender;
            ll.LinkVisited = true;
            System.Diagnostics.Process.Start("http://www.flickr.com/photos/gazeronly/8293918435/");
        }

    }
}
