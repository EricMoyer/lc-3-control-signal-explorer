﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LC_3ControlSignalExplorer
{
    /// <summary>
    /// Represents a control signal output by the LC-3 finite state machine or the values stored on a bus internal to the LC-3
    /// </summary>
    class ControlSignal
    {
        /// <summary>
        /// The random number generator that all control signal instances will share
        /// </summary>
        static Random random = new Random();

        /// <summary>
        /// The number of bits used by this control signal. Must be in the range [1..16] inclusive.
        /// </summary>
        UInt16 _numBits;

        /// <summary>
        /// Public property exposing numBits
        /// </summary>
        public UInt16 numBits { get { return _numBits; } }


        /// <summary>
        /// The value of the control signal as a string of <code>numBits</code> 1's and 0's (or as a text string in the case of an error)
        /// </summary>
        public String asString { get; set; }

        /// <summary>
        /// Return true if the string representation of this control signal is just 0's and 1's
        /// </summary>
        public Boolean isBitString{
            get
            {
                var r = new System.Text.RegularExpressions.Regex("[01]+");
                return r.IsMatch(asString);
            }
        }

        /// <summary>
        /// The value of the control signal if the bits are interpreted as an unsigned integer (a random number if the string is not composed of bits)
        /// </summary>
        public UInt16 asUInt16
        {
            get
            {
                try
                {
                    return Convert.ToUInt16(this.asString, 2);
                }catch(Exception)
                {
                    return (UInt16)((random.Next() & 0xFFFF0000) >> 16);
                }
            }

            set
            {
                String tmp = Convert.ToString(value, 2);
                if (tmp.Length < 16) { tmp = new String('0', 16 - tmp.Length) + tmp; }
                asString = tmp.Substring(tmp.Length - _numBits, _numBits);
            }
        }

        /// <summary>
        /// Creates a new control signal with <code>numBits</code> bits and all bits set to 0.
        /// </summary>
        /// <param name="numBits">The number of bits in this control signal. Must be in the range [1,16] inclusive.</param>
        public ControlSignal(UInt16 numBits)
        {
            if (numBits < 1 || numBits > 16)
            {
                throw new ArgumentOutOfRangeException("numBits", "numBits must be in the range [1..16]");
            }

            this._numBits = numBits;
            this.asString = new String('0', numBits);
        }

        /// <summary>
        /// Creates a new control signal with <code>numBits</code> bits where the bits take their values from the corresponding bits of initialValue.
        /// </summary>
        /// <example>(new ControlSignal(5,64+5)).asString == '00101'</example>
        /// <param name="numBits">The number of bits in this control signal. Must be in the range [1,16] inclusive.</param>
        /// <param name="initialValue">Bits in initialValue supply values for the corresponding initial values in the control signal</param>
        public ControlSignal(UInt16 numBits, UInt16 initialValue)
        {
            if (numBits < 1 || numBits > 16)
            {
                throw new ArgumentOutOfRangeException("numBits", "numBits must be in the range [1..16]");
            }

            this._numBits = numBits;
            this.asUInt16 = initialValue;
        }

        /// <summary>
        /// Creates a new control signal with <code>numBits</code> bits where the bits take their values from the initialValue.
        /// </summary>
        /// <example>(new ControlSignal(5,64+5)).asString == '00101'</example>
        /// <param name="numBits">The number of bits in this control signal. Must be in the range [1,16] inclusive.</param>
        /// <param name="initialValue">The initial value for the control signal. Will be cut or padded with zeros to be the right number of characters</param>
        public ControlSignal(UInt16 numBits, String initialValue)
        {
            if (numBits < 1 || numBits > 16)
            {
                throw new ArgumentOutOfRangeException("numBits", "numBits must be in the range [1..16]");
            }

            this._numBits = numBits;

            if (initialValue.Length < numBits)
            {
                this.asString = initialValue + new String('0', numBits - initialValue.Length);
            }
            else
            {
                this.asString = initialValue.Substring(0, numBits);
            }

        }
    }
}
